<?php include"query.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Tekanza Bonuses</title>
<meta charset="utf-8">
<meta name="description" content="Tekanza ICT Hub">
<meta name="keywords" content="Web Development, App Development, Network Infrastructure, Branding & Identity, Bonus offers">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="css/about.css">
<link rel="stylesheet" href="css/about_responsive.css">
<?php head();?>
<style>
	.just_1,.just_2{
	text-align:justify;
	}
	</style>
  <script src="preloader.js"></script>
  
<style>
    .hero-head{
        width:100%;
        height:420px;
        position:relative;
        background-color:rgba(10,50,100,.35);
    }
    .hero-head video{
        position:absolute;
        z-index:-1;
        width:100%;
        height:100%;
    }
    .head-overlay{
        position:absolute;
        z-index:10;
        width:100% !important;
        height:100% !important;
        background-color:rgba(10,50,100,.35);
        top:0;
        left:0;
        
    }
</style>
<div class="hero-head">
    <video playsinline style="pointer-events: none;" autoplay fullscreen muted controlsList="nodownload" loop="loop" poster="./images/tek_3.png">
      <source src="../folder/introBusinessEtEconomySansText_1591434698.mp4">
      Your browser does not support the video tag.
    </video>
    <div class="hero-overlay"></div>
</div>

 <div class="site-section">
    
     <div class="container mt-5">
         <div class="row">
            <div class="col-md-12">
                <div class="text-center py-5" style="font-size: 30px;">The Tekanza Bonus service program</div>
                <p> The Tekanza Bonus Service Program started as a SMEs empowerment program for Nigerians. Our scope is to empower individuals and communities around Nigeria and Africa as a whole, especially in Sub-Saharan Africa. <br/> Our Services...  </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center py-5" style="font-size: 30px;">Where we are</sub></div>
                Write up...
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 card p-3">
                <h5 class="py-2" style="font-size: 20px;">Who We Are?</h5>
                <p style="text-align: justify;">We are a technological and innovative enterprise and company, incorporated in Nigeria, with aim to
                    transform business operations, develop solutions to the growing needs of the society through the use
                    of technology and provide tools and platforms for digital awareness and engagements.</p>
            </div>
            <div class="col-md-6 card p-3">
                <h5 class="py-2" style="font-size: 20px;">What We Do</h5>
                <p style="text-align: justify;">As a technology company we plan and carry out boots-on-the-ground projects to accomplish our
                    objectives thereby engaging the synergy of innovative and creative minds in areas of Information
                    Technology, Application/Software Development, Computer Networking Technology, Web Development
                    Technology, Database Management Technology and Artificial Intelligence.</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="text-center py-5" style="font-size: 30px;">Our Geographical coverage <sub>(current - Nigeria)</sub></div>
                <img src="../folder/nigeria.svg" style="height:200px;" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center py-5" style="font-size: 30px;">Our Bonuses</div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">
                    <div class="card-body">
                        <h5 class="card-title">Ember/ New year bonus offer</h5>
                        <p class="card-text"> This is a general offer. It is designed to fit our several clients and users. Our goal here is to make this opportunity wide, proggressive and accommodating for business and individuals. This offer permeates our several offers. </p>
                    </div>
                    <a href="./bmOffer" class="btn btn-primary">Open</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">
                    <div class="card-body">
                        <h5 class="card-title">Mid-Year Offer </h5>
                        <p class="card-text">This is a general offer. It is designed to fit our several clients and users. Our goal here is to make this opportunity wide, proggressive and accommodating for business and individuals. This offer permeates our several offers.</p>
                    </div>
                    <a href="javascript:void();" class="btn btn-danger">Closed</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">
                <div class="card-body">
                    <h5 class="card-title">For Religious Organizations</h5>
                    <p class="card-text"> This is specifically crafted to empower religious institutions, especially churches and organizations wishing to go into the cyber space. This provides you the platform to manage religious events and activities digitally. </p>
                </div>
            <a href="javascript:void();" class="btn btn-danger">Closed</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">
                    <div class="card-body">
                        <h5 class="card-title">SMEs/Business Offer </h5>
                        <p class="card-text">Innovation is part of our DNA. We value original thinking and the passion to attack difficult challenges, seeking creative ways to solve tough problems.</p>
                    </div>
                    <a href="javascript:void();" class="btn btn-danger">Closed</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">
                <div class="card-body">
                    <h5 class="card-title"> For Schools and Institution.</h5>
                    <p class="card-text">Collaboration is basic human nature, Teamwork makes the dream work. We work together to bring our passions and expertise for the common good in commitment towards our goals and objectives.</p>
                </div>
            <a href="javascript:void();" class="btn btn-danger">Closed</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">
                <div class="card-body">
                    <h5 class="card-title"> For Non-Governmental Organizations</h5>
                    <p class="card-text">Collaboration is basic human nature, Teamwork makes the dream work. We work together to bring our passions and expertise for the common good in commitment towards our goals and objectives.</p>
                </div>
            <a href="javascript:void();" class="btn btn-danger">Closed</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
  </div>
    </div>
    </div>
    </div>
    
<!--<section class="site-section testimonial-wrap" data-aos="fade">
    What our clients/ beneficiaries are saying
</section>-->
<?php footer();?>
