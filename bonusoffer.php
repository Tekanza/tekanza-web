<!doctype html>
<head>
<title>Tekanza bonus offer</title>
<link rel="stylesheet" href="css/main2.css" /><link rel="stylesheet" href="css/bootstrap.min.css" />
<style>
body{width:100%;}
.toppy{padding:2px 15px;}
.header{
    width:100%;
    padding:15px 5px;
    background:#444;
    color:#fff;
    position:sticky;
    top:0;
	z-index:1000;
}
.headimage{
    width:100%;
    height:150px;
    border-bottom:1.5px solid #048bff;
    margin-bottom:25px;
}
.headimage img{
    display:block;
    max-width:850px;
    margin:0 auto;
    width:100%;
    height:100%;
}

.acd{
  background-color: transparent;
  color: #444;
  cursor: pointer;
  border:1.5px solid #bee5eb;
  padding: 18px;
  width: 100%;
  text-align: left;
  outline: none;
  border-radius:8px;
  transition: 0.4s;
}

.acti, .acd:hover{
  background-color: rgba(0,0,0,.2);
}

.pv{
  padding: 0 18px;
  background-color: transparent;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}
.acd:after {
  content: '\02795';
  font-size: 13px;
  color: #777;
  float: right;
  margin-left: 5px;
}

.acti:after {
  content: "\2796";
}
</style>
</head>
<body>
<div class="toppy"><a href="mailto:info@tekanza.com">info@tekanza.com</a>|<a href="mailto:hi@myocoms.com">hi@myocoms.com</a>|<small>Support - </small><a href="tel:+234-9060976211">+234-9060976211</a></div>
<div class="header">
    <img style="width:150px;height:22px;" src="./images/tek_3.png" />
</div>
<style>
    .hero-head{
        width:100%;
        height:620px;
        position:relative;
    }
    .hero-head> video{
        position:absolute;
        z-index:1;
        width:100%;
        height:100%;
    }
    .head-overlay{
        position:absolute;
        z-index:10;
        width:100%;
        height:100%;
        background-color:rgba(10,50,100,.35);
    }
</style>
<div class="hero-head">
    <video controls="off" autoplay fullscreen controlsList="nodownload" loop="loop" poster="./images/tek_3.png">
      <source src="folder/movie.mov">
      <!--<source src="folder/movie.mp4" type="video/mp4">-->
      Your browser does not support the video tag.
    </video>
    <div class="hero-overlay"></div>
</div>

<div class="container alert alert-info">
    <h2>Tekanza bonus offer</h2>
    <p><b>Why Do I need a Website???</b><br>
     There comes a time for every business to move forward, expand and take its proper place in the market and meet more audience. With each sale and deal, every company earns more experience and gains new opportunities to grow and expand its business acumen. While there is always a possibility for you to build up and move your business forward, it is imperative to know that it is hard even for a professional to run a business solely. But fortunately, there is another way which will give you many opportunities and will lift boost business to a whole new level. Here are some reasons to show how important it is to have your website: </p>

<button class="acd"><i class="fa fa-cart-plus"></i> #1 – Online brochure</button>
<div class="pv">
  <p>Companies spend millions creating brochures and distributing them. By having a website you can skip that entirely. Your potential customers can find out about you and any of your products online. If you get most of your business through networking and personal connections, then they will want to check out your website. </p>
</div>
<button class="acd"><i class="fa fa-cart-plus"></i> #2 – More customers</button>
<div class="pv">
  <p> More than 2.4 billion people use the internet every day, and some 90% of those have purchased something or contacted a company, online in the last 12 months. So by not having a website, you will be missing out on a big piece of the pie. </p>
</div>
<button class="acd"><i class="fa fa-cart-plus"></i> #3 – Business value</button>
<div class="pv">
  <p> Have you tried getting a business loan recently? It’s not easy, but if you try and the bank manager asks to see your website, you better have a pretty good one. It doesn’t just stop with the bank; the perceived value of your business will be lower in everyone’s eyes – especially your customers. </p>
</div>
<button class="acd"><i class="fa fa-cart-plus"></i> #4 – Influence</button>
<div class="pv">
  <p>By having a website potentially, thousands of people are going to see it. You can influence people’s decisions and educate them.</p>
<p> We can help you achieve all these.</p>
</div>
<button class="acd"><i class="fa fa-cart-plus"></i> #5 – Lock down/ Curfew</button>
<div class="pv">
  <p>Incase of Lockdowns and Curfew keeping your customers should be top priority.</p>
<p> We can help you achieve these.</p>
</div>

    <p>Tekanza LLC is interested to provide the best professional website platform for businesses through it bonus program. We encourage you to comply and provide the necessary information we require to start your project. Time frame for each project will last for at most two (2) weeks.</p>
    <p>This collaborative initiative is to assist SMEs, entrepreneurs and individuals in Nigeria and to provide a platform that build their digital engagements and awareness. Our goal is to enhance the digital productivity of Nigerians while spending less this End of Year/ New Year.</p>
    <p>Taking your business online is one of the most convenient ways of giving your customers the best business experience & relationship with your enterprise.</p>
    <marquee><b>This offer is for Nigerians only!</b></marquee>
</div>
<center><img src="images/tk.jpg" style="max-width:100%;max-height:150px" /></center>
<div class="container alert alert-info">
<p>How it works:</p>
<ol class="ol">
<li>Please read the instructions carefully.</li>
<li>Click on continue at the bottom of this page.</li>
<li>You will be redirected to a form, fill the form with your preferred information where necessary. (ensure to fill the right information as required)</li>
<li>After filling the form a Payment page will popup, continue with payment so as to successfully send your request to our standby team.</li>
<li>Once payment is completed, our developers will get back to you and start your project immediately. <br/> <b> N/B: </b> Payment is one time only.</li>
<li>Your Project should be ready on or before the fall of two (2) weeks.</li>
</ol>

<div class="container alert alert-info">
<p>Benefits of this offer:</p>
<ol class="ol">
<li>One year free developer support.</li>
<li>Andriod App for your Websites and for your users.</li>
<li>Site admin panel inclusive.</li>
<li>SEO visibility.</li>
<li>.com or .com.ng domain names with dedicated Severs (i.e. no downtime).</li>
<li>Free payment gateway for e-commerce websites.</li>
<li>Free Custom Emails</li>
</ol>
</div>
<div class="no_t wait">
    <style>
        .blink{
            transition:.8s ease-in-out;
            animation:blink .8s infinite;
        }
        @keyframes blink{
            from{opacity:1;}
            to{opacity:0;}
        }
        .cnt{
            padding:20px;
            width:100%;
            max-width:600px;
            display:block;
            margin:0 auto;
        }
    </style>
<center><h3 style="color:#0000ff;" class="blink">Now OPEN</h3>
<h4 style="color:#900;">20th November, 2020 - 20th Febuary, 2021</h4></center>
</div>
<a href="forms/bonusoffer?v=<?= time(); ?>" class="btn cnt btn-primary">Continue</a>
</div>
<script> var acc = document.getElementsByClassName("acd"); var i; for (i = 0; i < acc.length; i++) { acc[i].addEventListener("click", function() { this.classList.toggle("acti"); var panel = this.nextElementSibling; if (panel.style.maxHeight) { panel.style.maxHeight = null; } else { panel.style.maxHeight = panel.scrollHeight + "px"; }  }); } </script>
</body>
</html>