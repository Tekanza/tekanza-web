<?php include"query.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Networking Infrastructure</title>
<meta charset="utf-8">
<meta name="description" content="Tekanza ICT Hub">
<meta name="keywords" content="Web Development, App Development, Network Infrastructure, Branding & Identity">
<meta name="author" content="John Robert, Aniefiok Udoh, Emmanuel Ekunke">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="css/about">
<link rel="stylesheet" href="css/about_responsive">
<?php head();?>
    <div class="site-blocks-cover overlay" style="background-image: url(images/tek-bg-img_1.png); background-size: cover;" data-aos="fade">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-12" data-aos="fade-up" data-aos-delay="400">
                        
            <div class="row justify-content-center mb-4">
              <div class="col-md-8 text-center">
                <h1 class="cap" style="text-transform:uppercase;"><b> Networking Infrastructure </b></h1>
				 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> 
<div class="site-section container">
<div class="row justify-content-center">
<h2 align="center" style="color:#ad3335;">"We provide the ease by creating the best network infrastructure for your home, business and enterprise."</h2>
<div class="col md-12">
<p>Tekanza ICT Hub is a sophisticated network infrastructure company designed to promote and increase the productivity of any enterprise through quality network services delivery. As a business enterprise, government and any institution understanding the importance of network infrastructure design and developing an implementation plan based on the needs and priorities of your firm is very crucial to long-term success. Therefore, hiring and partnering with Tekanza will provides a reliable network infrastructure plan to you while giving the flexibility to outsource and augment your company services optimally.</p> <p>Our team is well equipped with the infrastructural design and development and they can manage your network infrastructure design and implementation from the ground up, linking and connecting multiples network infrastructural devices, bringing locations and powering the business systems your company needs to build.</p>
<p>Our network infrastructure consultants and engineers have years of experience in architecting, managing network cloud system, servers and client infrastructure, on-premises, etc that are designed to the specific business goals and models. Every infrastructure designed is uniquely given to each client aims at individual goals and objectives for proper optimized operations. Tekanza has an expertise in development and implementing solutions.</p></div>

<div class="col-md-12"><h4 class="text-black h1 site-section-heading text-center">Read more on some of our Networking Infrastructure services below:</h4></div>
        </div>

<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> Server and Client installation</i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS.</p>
</div>

<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> On-premise server computing and storage</i></button> 
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS.</p>
</div>

<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> VOIP and on-premises telephone system</i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS.</p>
</div>

<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> Implementing and managing of cloud computing and storage.</i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS.</p>
</div>

<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> Business continuity, disaster recovery and data back-up</i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS.</p>
</div>

<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> Office relocation or new location built-out</i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS</p>
</div>

<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> Centralized prints services</i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS</p>
</div>
<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> IP Surveillance </i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS.</p>
</div>
<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> Cabling infrastructure</i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS.</p>
</div>
<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> Physical and logical connectivity</i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS.</p>
</div>
<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> Wireless installation and management</i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS.</p>
</div>
<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> Email services</i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS.</p>
</div>
<button class="acd1"><i class="fa fa-circle" style="font-size:1em;"> Firewall, antivirus, antispam and data security management. </i></button>
<div class="pv">
  <p>DAASSHSHSHSHSSHSHSHSHSHSHSHSHSHSHSHSHS.</p>
</div>
</div>
<!---------------------------------------------------------
<div class="row_r_1">
<div class="column_c_1">
<div class="flip-box_1">
  <div class="flip-box-inner">
    <div class="flip-box-front">
      <h2><a href="#School Management Website/"><i class="fa fa-institution"> School Management Website</i></a></h2>
    </div>
    <div class="flip-box-back">
      <h2><a href="#School Management Website/"><i class="fa fa-institution"> School Management Website</i></a></h2>
    </div>
  </div>
</div>
</div>

<div class="column_c_1">
<div class="flip-box_1">
  <div class="flip-box-inner">
    <div class="flip-box-front">
      <h2><a href="#Hospital/Pharmaceutical Management/"><i class="fa fa-medkit"> Hospital/Pharmaceutical Management</i></a></h2>
    </div>
    <div class="flip-box-back">
      <h2><a href="#Hospital/Pharmaceutical Management/"><i class="fa fa-medkit"> Hospital/Pharmaceutical Management</i></a></h2>
    </div>
  </div>
</div>
</div>
</div>
<div class="column_c_1">
<div class="flip-box_1">
  <div class="flip-box-inner">
    <div class="flip-box-front">
      <h2><a href="# Hotels Design &amp; Management/"><i class="fa fa-hotel"> Hotels Design &amp; Management</i></a></h2>
    </div>
    <div class="flip-box-back">
      <h2><a href="# Hotels Design &amp; Management/"><i class="fa fa-hotel"> Hotels Design &amp; Management</i></a></h2>
    </div>
  </div>
</div>
</div>
<div class="column_c_1">
<div class="flip-box_1">
  <div class="flip-box-inner">
    <div class="flip-box-front">
      <h2><a href="#Fashion Design/"><i class="fa fa-shopping-bag"> Fashion Design</i></a></h2>
    </div>
    <div class="flip-box-back">
      <h2><a href="#Fashion Design/"><i class="fa fa-shopping-bag"> Fashion Design</i></a></h2>
    </div>
  </div>
</div>
</div>
</div>

<div class="column_c_1">
<div class="flip-box_1">
  <div class="flip-box-inner">
    <div class="flip-box-front">
      <h2><a href="#E-commerce site Development/"><i class="fa fa-cart-plus"> E-commerce site Development</i></a></h2>
    </div>
    <div class="flip-box-back">
      <h2><a href="#E-commerce site Development/"><i class="fa fa-cart-plus"> E-commerce site Development</i></a></h2>
    </div>
  </div>
</div>
</div>

<div class="column_c_1">
<div class="flip-box_1">
  <div class="flip-box-inner">
    <div class="flip-box-front">
      <h2><a href="#Automobile & Transport Company Design and management/"><i class="fa fa-automobile"> Automobile & Transport Company Design and management</i></a></h2>
    </div>
    <div class="flip-box-back">
      <h2><a href="#Automobile & Transport Company Design and management/"><i class="fa fa-automobile"> Automobile & Transport Company Design and management</i></a></h2>
    </div>
  </div>
</div>
</div>
</div>
<div class="column_c_1">
<div class="flip-box_1">
  <div class="flip-box-inner">
    <div class="flip-box-front">
      <h2><a href="#Ministry of Interior Design & Management/"><i class="fa fa-fighter-jet"> Ministry of Interior Design & Management</i></a></h2>
    </div>
    <div class="flip-box-back">
      <h2><a href="#Ministry of Interior Design & Management/"><i class="fa fa-fighter-jet"> Ministry of Interior Design & Management</i></a></h2>
    </div>
  </div>
</div>
</div>
<div class="column_c_1">
<div class="flip-box_1">
  <div class="flip-box-inner">
    <div class="flip-box-front">
      <h2><a href="#Multimedia Design & Blogging/"><i class="fa fa-toggle-right"> Multimedia Design & Blogging</i></a></h2>
    </div>
    <div class="flip-box-back">
      <h2><a href="#Multimedia Design & Blogging/"><i class="fa fa-toggle-right"> Multimedia Design & Blogging</i></a></h2>
    </div>
  </div>
</div>
</div>
</div>
<div class="bott_om">
</div>
---->

 <section class="site-section testimonial-wrap">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-8 text-center">
            <h2 class="text-black h1 site-section-heading text-center">Testimonials</h2>
		</div>
        </div>
      </div>

      <div class="slide-one-item home-slider owl-carousel">
          <div>
		  
            <div class="testimonial">
              
              <blockquote class="mb-5">
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>

              <figure class="mb-4 d-flex align-items-center justify-content-center">
                <div><img src="images/person_3.jpg" alt="Image" class="w-50 img-fluid mb-3"></div>
                <p>John Smith</p>
              </figure>
            </div>
          </div>
          <div>
            <div class="testimonial">

              <blockquote class="mb-5">
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>
              <figure class="mb-4 d-flex align-items-center justify-content-center">
                <div><img src="images/person_2.jpg" alt="Image" class="w-50 img-fluid mb-3"></div>
                <p>Christine Aguilar</p>
              </figure>
              
            </div>
          </div>

          <div>
            <div class="testimonial">

              <blockquote class="mb-5">
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>
              <figure class="mb-4 d-flex align-items-center justify-content-center">
                <div><img src="images/person_4.jpg" alt="Image" class="w-50 img-fluid mb-3"></div>
                <p>Robert Spears</p>
              </figure>
           
            </div>
          </div>

          <div>
            <div class="testimonial">

              <blockquote class="mb-5">
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>
              <figure class="mb-4 d-flex align-items-center justify-content-center">
                <div><img src="images/person_5.jpg" alt="Image" class="w-50 img-fluid mb-3"></div>
                <p>Bruce Rogers</p>
              </figure>

            </div>
          </div>
        </div>
    </section>
	<!-- Featured Project  Page-->
     <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center border-primary">
         <h2 style="color:#000" class="" data-aos="fade">Recent Projects</h2>
          </div>
        </div>
      </div>
<div class="main_feap">
<div class="row_feap">
  <div class="column_feap">
    <div class="content_feap">
      <img src="images/img_h_1.png" alt="" style="width:100%">
    </div>
  </div>
    <div class="column_feap">
    <div class="content_feap">
      <img src="images/img_h_1.png" alt="" style="width:100%">
      </div>
  </div>
</div>
</div>
<div class="main_feap">
<div class="row_feap">
  <div class="column_feap">
    <div class="content_feap">
      <img src="images/img_h_1.png" alt="" style="width:100%">
    </div>
  </div>
    <div class="column_feap">
    <div class="content_feap">
      <img src="images/img_h_1.png" alt="" style="width:100%">
      </div>
  </div>
</div>
</div>
<center>
<div class="s_show_all">
<button class="s_show_all_1"><a href="#Show_All/">Show All <i class="fa fa-sort-down" style="font-size:1.8em;"></i></a></button>
</div></center>
<?php footer();?>
<script> var acc = document.getElementsByClassName("acd1"); var i; for (i = 0; i < acc.length; i++) { acc[i].addEventListener("click", function() { this.classList.toggle("acti"); var panel = this.nextElementSibling; if (panel.style.maxHeight) { panel.style.maxHeight = null; } else { panel.style.maxHeight = panel.scrollHeight + "px"; }  }); } </script>