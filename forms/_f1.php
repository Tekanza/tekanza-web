<?php
session_start();
require "ccheck.php";

if(!defined('tinyEMFATI_AI_check')){
	header("HTTP/1.0 404 Error");
	readfile('./404.php');
	exit();
}

require "PHPMailer/PHPMailerAutoload.php";
date_default_timezone_set("Africa/Lagos");

function smtpmailer($to, $from, $from_name, $subject, $body)
    {
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPAuth = true; 
 
        $mail->SMTPSecure = 'ssl'; 
        $mail->Host = 'mail.tekanza.com';
        $mail->Port = 465;  
        $mail->Username = 'support@tekanza.com';
        $mail->Password = '#tekanza19';   
   
   //   $path = 'reseller.pdf';
   //   $mail->AddAttachment($path);
   
        $mail->IsHTML(true);
        $mail->From= $from;
        $mail->FromName=$from_name;
        $mail->Sender=$from;
    //  $mail->AddReplyTo($from, $from_name);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AddAddress($to);
        if(!$mail->Send())
        {
            $error ="Please try Later, Error Occured while Processing...";
            return $error; 
        }
        else 
        {
            $error = "Thanks You !! Your email is sent.";  
            return $error;
        }
    }

class database{
	private $conn;
	public $username = "tekanzac";
	public $dbname = "tekanzac_bonuses";
	public $password = "#tekanza19";
	public $host = "localhost";
	public function getDbconnection(){
		$this->conn = null;
		try{
			$this->conn = new PDO("mysql:host=".$this->host. ";dbname=".$this->dbname, $this->username, $this->password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
		}
		catch(PDOException $e){
			$e->getMessage();
		}
		return $this->conn;
	}
}

$dbase = new database;
$db = $dbase->getDbconnection();

function isSiteAvailible($url){
     $url_headers = @get_headers($url);
     if(strpos($url_headers[0],'200'))
     {
      $exist = TRUE;
     }
     else 
     {
      $exist = FALSE;
     }
    return $exist;
}

if(isset($_GET["i"])){
$domain = $_GET['i'];
if ( gethostbyname($domain) != $domain ) {
    $code = 1;
    $response = "<div class=\"alert alert-danger\">Aww <b>Unavailable</b>, So sorry, this domain name already exist, please try using another phrase or word. <small>(It's recommended you use a name relating to your business name.)</small>.</div>";
}
else {
    $code = 0;
    $response = "<div class=\"alert alert-success\">Yes <b>Available</b>!, Congratulations this domain name might be yours soon. <small>(It will be put into consideration. It's recommended you use a name relating to your business name.)</small>.</div>";
}

    print json_encode(["c"=> $code, "i"=>$response]);
	exit();
}

if(isset($_POST["fieldinput"])){

#personal info
	$fullname = @$_POST["fieldinput"]["pInfo"][0];
	$emailAddress = @$_POST["fieldinput"]["pInfo"][1];
	$phoneNumber = @$_POST["fieldinput"]["pInfo"][2];
	$personalAddress = @$_POST["fieldinput"]["pInfo"][3];
	$country = @$_POST["fieldinput"]["pInfo"][4];
	$state = @$_POST["fieldinput"]["pInfo"][5];

#request info
	$typeofwebsite = @$_POST["fieldinput"]["rInfo"][0];
	
#additional pages
	$item1 = @$_POST["fieldinput"]["rInfo"][1];
	$item2 = @$_POST["fieldinput"]["rInfo"][2];
	$item3 = @$_POST["fieldinput"]["rInfo"][3];
	$item4 = @$_POST["fieldinput"]["rInfo"][4];
	$item5 = @$_POST["fieldinput"]["rInfo"][7];

#preferred template
	$template = @$_POST["fieldinput"]["rInfo"][5];

#preferred domain name
	$uri = @$_POST["fieldinput"]["rInfo"][6];
	$app = @$_POST["fieldinput"]["rInfo"][8];

#business info
	$businessName = @$_POST["fieldinput"]["bInfo"][0];
	$businessEmail = @$_POST["fieldinput"]["bInfo"][1];
	$businessPhone = @$_POST["fieldinput"]["bInfo"][2];
	$businessAddress = @$_POST["fieldinput"]["bInfo"][3];
	$businessCategory = @$_POST["fieldinput"]["bInfo"][4];
	$rc_bn_name = @$_POST["fieldinput"]["bInfo"][5][0];
	$rc_bn = @$_POST["fieldinput"]["bInfo"][5][1];

#controllers email
	$controlEmail = @$_POST["fieldinput"]["cInfo"][0];
	$controlPassword = @$_POST["fieldinput"]["cInfo"][1];

#acceepted t&c's
	$agreed = @$_POST["fieldinput"]["accept"][0];
	
#code
	$code = "TZb-".rand(1000000, 9999999);
	
#reference code
	$ref = @$_SESSION["tekky_ref"];
//echo json_encode($data);
	$data = [
		'code' => $code,
		'ref' => ($ref ? $ref : "Nil"),
		'fullname' => $fullname,
		'emailAddress' => ($emailAddress ? $emailAddress : $controlEmail),
		'phoneNumber' => $phoneNumber,
		'personalAddress' => $personalAddress,
		'country' => $country,
		'state' => ucwords( $state ),
		'businessName' => ($businessName ? $businessName : "Nil"),
		'businessEmail' => ($businessEmail ? $businessEmail : "Nil"),
		'businessPhone' => ($businessPhone ? $businessPhone : "Nil"),
		'businessAddress' => ($businessAddress ? $businessAddress : "Nil"),
		'businessCategory' => ucfirst( ($businessCategory ? $businessCategory : "Nil") ),
		'rc_bn' => ($rc_bn_name ? $rc_bn_name : "RC").($rc_bn ? $rc_bn : ""),
		'controlEmail' => $controlEmail,
		'controlPassword' => $controlPassword,
		'typeofwebsite' => ucfirst( $typeofwebsite ),
		'template' => ($template ? $template : "Nil"),
		'domainName' => $uri,
		'app' => ($app ? "Yes" : "No"),
		'pg1' => ucfirst( "home" ),
		'pg2' => ucfirst( "about" ),
		'pg3' => ucfirst( "contact" ),
		'pg4' => ucfirst( ($item1 ? $item1 : "Nil") ),
		'pg5' => ucfirst( ($item2 ? $item2 : "Nil") ),
		'pg6' => ucfirst( ($item3 ? $item3 : "Nil") ),
		'pg7' => ucfirst( ($item4 ? $item4 : "Nil") ),
		'pg8' => ucfirst( ($item5 ? $item5 : "Nil") ),
		'date' => date("D d-M-Y H.i.s A"),
		'year' => date("Y"),
		'agreed' => $agreed
	];
	//echo json_encode($data);
	session_destroy();
	
	$query = $db->prepare("INSERT INTO emberoffer SET
		code = :code,
		ref = :ref,
		fullname = :fullname,
		emailAddress = :emailAddress,
		phoneNumber = :phoneNumber,
		personalAddress = :personalAddress,
		country = :country,
		state = :state,
		businessName = :businessName,
		businessEmail = :businessEmail,
		businessPhone = :businessPhone,
		businessAddress = :businessAddress,
		businessCategory = :businessCategory,
		rc_bn = :rc_bn,
		controlEmail = :controlEmail,
		controlPassword = :controlPassword,
		typeofwebsite = :typeofwebsite,
		template = :template,
		domainName = :domainName,
		want_app = :app,
		pg1 = :pg1,
		pg2 = :pg2,
		pg3 = :pg3,
		pg4 = :pg4,
		pg5 = :pg5,
		pg6 = :pg6,
		pg7 = :pg7,
		pg8 = :pg8,
		date = :date,
		year = :year,
		agreed = :agreed
	")->execute($data);

	$mail_body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Tekanza Confirmation Email</title>
    <style type="text/css">
    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
    a img { border: none; }
    table { border-collapse: collapse !important; }
    #outlook a { padding:0; }
    .ReadMsgBody { width: 100%; }
    .ExternalClass {width:100%;}
    .backgroundTable {margin:0 auto; padding:0; width:100%;!important;}
    table td {border-collapse: collapse;}
    .ExternalClass * {line-height: 115%;}
    td {
      font-family: Arial, sans-serif;
      color: #5e5e5e;
      font-size: 16px;
      text-align: left;
    }
    body {
      -webkit-font-smoothing:antialiased;
      -webkit-text-size-adjust:none;
      width: 100%;
      height: 100%;
      color: #5e5e5e;
      font-weight: 400;
      font-size: 16px;
    }
    h1 {
      margin: 10px 0;
    }
    a {
      color: #2b934f;
      text-decoration: none;
    }
    .body-padding {
      padding: 0 75px;
    }
    .force-full-width {
      width: 100% !important;
    }

    .icons {
      text-align: right;
      padding-right: 30px;
    }

    .logo {
      text-align: left;
      padding-left: 30px;
    }

    .computer-image {
      padding-left: 30px;
    }

    .header-text {
      text-align: left;
      padding-right: 30px;
      padding-left: 20px;
    }
    .header {
      color: #232925;
      font-size: 24px;
    }
    </style>
    <style type="text/css" media="screen">
        @media screen {
          * {
            font-family: PT Sans, Helvetica Neue, Arial, sans-serif !important;
          }
        }
    </style>
    <style type="text/css" media="only screen and (max-width: 599px)">
        @media only screen and (max-width: 599px) {
        table[class*="w320"] {
          width: 320px !important;
        }
        td[class*="icons"] {
          display: block !important;
          text-align: center !important;
          padding: 0 !important;
        }
        td[class*="logo"] {
          display: block !important;
          text-align: center !important;
          padding: 0 !important;
        }
        td[class*="computer-image"] {
          display: block !important;
          width: 230px !important;
          padding: 0 45px !important;
          border-bottom: 1px solid #e3e3e3 !important;
        }
        td[class*="header-text"] {
          display: block !important;
          text-align: center !important;
          padding: 0 25px!important;
          padding-bottom: 25px !important;
        }
        *[class*="mobile-hide"] {
          display: none !important;
          width: 0 !important;
          height: 0 !important;
          line-height: 0 !important;
          font-size: 0 !important;
        }
      }
    </style>
  </head>
  <body offset="0" class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
  <p style="display:none;">Welcome to Tekanza LLC. I would like to inform you that your request has been recieved and currently been worked on!</p>
  <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
    <tr>
      <td align="center" valign="top" style="background-color:#ffffff" width="100%">

      <center>
        <table cellspacing="0" cellpadding="0" width="600" class="w320">
          <tr>
            <td align="center" valign="top">

              <table class="force-full-width" cellspacing="0" cellpadding="0" style="background-color:#ffffff"" bgcolor="#ffffff">
                <tr>
                  <td style="background-color:#ffffff" class="logo">
                    <br>
                    <a href="https://tekanza.com/bonusoffer" style="color:#444444"><img style="width:250px;max-height:100px;" src="https://tekanza.com/images/tek_3.png" alt="Tekanza"></a>
                  </td>
                </tr>
                <tr>
                  <td class="icons">
                    <p style="font-size:13px;"><i>Think out of the box!</i></p>
                  </td>
                </tr>
              </table>

              <table cellspacing="0" cellpadding="0" class="force-full-width" bgcolor="#ffffff">
                <tr>
                  <td style="color: #444;" class="header-text"><br><br>
                    <span style="font-size: 24px;">Congratulations!</span><br>
                    <br><span style="font-size: 14px;">
					Hello <b>'. explode(" ", $data["fullname"])[0].'</b>,<br><br>
					<b>Oril</b> here (Co-Founder),<br><br>
                    I would like to inform you that your request has been recieved and currently been worked on! More details below.
                    <br>
                    <br></span>
                  </td>
                </tr>
              </table>
			  
              <table class="force-full-width" cellspacing="0" cellpadding="30" style="marign-top:10px;" bgcolor="#ffffff">
                <tr>
                  <td>
                    <table cellspacing="0" cellpadding="20" class="force-full-width">
                      <tr>
                        <td style="background-color:#ffffff;padding-bottom: 5px; font-weight:bold; ">
                         Personal info
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <span class="header" style="font-size: 14px;">Name: '.$data["fullname"].'</span> <br>
                          Phone: '.$data["phoneNumber"].',<br>
                          Email: '.$data["emailAddress"].',<br>
                          Address: '.$data["personalAddress"].',<br>
                          State/Country: '.$data["state"].',&nbsp;'.$data["country"].' <br><br>
                        </td>
                        <td style="text-align:left; vertical-align:top;">
                          <span class="header" style="font-size: 13px;">Reference Code: '.$data["code"].'</span><br>
                          Payment Reference: '.$data['ref'].'<br>
                          Generated on: '.$data["date"].' <br><br>
                        </td>
                      </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" style="margin-top:10px;font-size:14px;" class="force-full-width">
                      <tr>
                        <td style="background-color:#ffffff; padding: 5px; font-weight:bold; ">
                          Request details
                        </td>
                      </tr>
                      <tr>
                        <td style="background-color:#ffffff; padding: 10px 5px;">
                          Type of Website: '.$data["typeofwebsite"].'<br>
                          Website template: '.$data["template"].'<br>
                          Additional pages (basic): Home, About & Contact<br>
                          Pages added by you: '.$data["pg4"].',&nbsp;'.$data["pg5"].',&nbsp;'.$data["pg6"].',&nbsp;'.$data["pg7"].',&nbsp;'.$data["pg8"].'<br>
                          Preferred domain name: '.$data["domainName"].'<br><br>
                          Need an Andriod app?: '.$data["app"].'<br><br>
                          Business/ Company Name: '.$data["businessName"].'<br>
                          Business/ Company email: '.$data["businessEmail"].'<br>
                          Business/ Company phone: '.$data["businessPhone"].'<br>
                          Business/ Company address/ Personal address: '.$data["businessAddress"].'<br>
                          Business/ Company category: '.$data["businessCategory"].'<br>
                          RC/ BN: '.$data["rc_bn"].'<br><br>
                          Web hosting authentication email: '.$data["controlEmail"].'<br>
                          Web hosting authentication password: '.$data["controlPassword"].'<br>
                        </td>
                      <tr>
                    </table>
                  </td>
                </tr>
              </table>

              <table class="force-full-width" cellspacing="0" cellpadding="20" style="background-color:#009fd1;" bgcolor="#009fd1">
                <tr>
                  <td style="background-color:#009fd1; color:#ffffff; font-size: 12px; text-align: center;">
                    <p style="font-size:10px">Warm regards from all of us at Tekanza. Please for complaints and support you can reach us via<br>phone: +234-9060-9762-11<br>or via mail: support@tekanza.com</p>
                  </td>
                </tr>
                <tr>
                  <td class="">
                    <a href="https://facebook.com/tekanz" style="color:#ffffff;font-size:12px;"><img srcc="https://1000logos.net/wp-content/uploads/2016/11/Facebook-logo.png" style="width:50px;height:50px;"/> </a>
                  </td>
                </tr>
                <tr>
                  <td style="background-color:#009fd1; color:#ffffff; font-size: 12px; text-align: center;">
                    Tekanza © '.$data["year"].' All Rights Reserved
                  </td>
                </tr>
              </table>


            </td>
          </tr>
        </table>

      </center>
      </td>
    </tr>
  </table>
  </body>
</html> ';

    smtpmailer($data["emailAddress"],"noreply@tekanza.com", "Tekanza" ,"Tekanza", $mail_body);
    if($query){
		echo json_encode([
		        "c" => $code,
		    ]);
	}
	exit();
}



