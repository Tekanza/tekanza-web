<?php
#Hi from Tekanza Developers.

#This script  was designed, debugged and administered by Tekanza.
#This script is licensed by, and remains a trademark of Tekanza.

#If errors occur please do contact Tekanza Developers via mail @" hi.developers@tekanza.com ".
#If you are using .NET Server please do contact Tekanza Developers via mail @" hi.developers@tekanza.com " as quick as possible.

#© Tekanza LLC. https://tekanza.com.

#Nothing should be editted under this Line as we have made sure no errors should be encountered.
#If errors should arise due to unauthorized editting, service charges will arise.

#error_reporting(0);

//Database
class database{
	private $conn;
	public $username = "root";
	public $dbname = "tekky_personal_site";
	public $password = "";
	public $host = "localhost";
	public function getDbconnection(){
		$this->conn = null;
		try{
			$this->conn = new PDO("mysql:host=".$this->host. ";dbname=".$this->dbname, $this->username, $this->password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
		}
		catch(PDOException $e){
			$e->getMessage();
		}
		return $this->conn;
	}
}

$dbase = new database;
$db = $dbase->getDbconnection();

$db_connect = mysqli_connect("localhost","root","","tekky_personal_site");

//General Website Info
$web_data = mysqli_query($db_connect,"SELECT * FROM web_settings");
while($d = mysqli_fetch_array($web_data)){
	$company_name	= $d["company_name"];
	$logo					=	$d["company_logo"];
	$contact_number	= $d["company_number"];
	$contact_email		= $d["company_email"];
	$open_hours			= $d["open_hours"];
	$theme_color		= $d["theme_color"];
	$currency				= $d["company_currency"];
	$contact_address	= $d["contact_address"];
	$about_us				= $d["about_us"];
	$partners				=	$d["partners"];
	$footer_about_us	=  substr($about_us,0,100);
	$contactPageImage	=	$d["contact_page_image"];
	$instagram				=	$d["social_instagram"];
	$facebook				=	$d["social_facebook"];
	$twitter					=	$d["social_twitter"];
	$powered_by		= "<a href=\"https://tekanza.com\">Tekanza.</a>";
	$year					= date("Y");
}
//End of General Info

//Website Parts
function headTags(){
	return '<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="manifest" href="site.webmanifest">
	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/slicknav.css">
	<link rel="stylesheet" href="assets/css/flaticon.css">
	<link rel="stylesheet" href="assets/css/animate.min.css">
	<link rel="stylesheet" href="assets/css/magnific-popup.css">
	<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="assets/css/themify-icons.css">
	<link rel="stylesheet" href="assets/css/slick.css">
	<link rel="stylesheet" href="assets/css/nice-select.css">
	<link rel="stylesheet" href="assets/css/style.css">';
}
function webJs(){
	
}
function navLinks(){
	global $logo;
	return'<div class="header-area">
                <div class="main-header  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-2 col-lg-2 col-md-1">
                                <div class="logo">
                                    <a href="../"><img src="assets/img/'.$logo.'" alt=""></a>
                                </div>
                            </div>
                            <div class="col-xl-10 col-lg-10 col-md-10">
                                <div class="menu-main d-flex align-items-center justify-content-end">
                                    <!-- Main-menu -->
                                    <div class="main-menu f-right d-none d-lg-block">
                                        <nav> 
                                            <ul id="navigation">
                                                <li><a href="../">Home</a></li>
                                                <li><a href="about.php">About</a></li>
                                                <li><a href="contact.php">Contact</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="header-right-btn f-right d-none d-xl-block ml-20">
                                        <a href="#" class="btn header-btn">Get Free Consultent</a>
                                    </div>
                                </div>
                            </div>   
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>';
}
function intro(){
	$data = "";
	$data .= '
				<!--<span data-animation="fadeInUp" data-delay=".4s">Get Every Single Solutions.</span>-->
				<h2 data-animation="fadeInUp" data-delay=".6s">I’m Designer Haris F. Watson <br />This wait wait wait wait wait wait wait wait wait wait wait wait wait wait wait wait wait wait wait.</h2>';
	$data .= '';
	
	return $data;
}

function webFooter(){
	global $logo;
	global $company_name;
	global $contact_number;
	global $contact_email;
	global $contact_address;
	global $open_hours;
	global $footer_about_us;
	global $instagram;
	global $facebook;
	global $twitter;
	global $powered_by;
?>
	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center">
						<div class="newsletter_title_container">
							<div class="newsletter_icon"><img src="images/send.png" alt=""></div>
							<div class="newsletter_title">Sign up for Newsletter</div>
							<div class="newsletter_text"><p>...and receive %20 discount for first shopping.</p></div>
						</div>
						<div class="newsletter_content clearfix">
							<form action="#" class="newsletter_form">
								<input type="email" class="newsletter_input" required="required" placeholder="Enter your email address">
								<button class="newsletter_button">Subscribe</button>
							</form>
							<div class="newsletter_unsubscribe_link"><a href="#">unsubscribe</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 footer_col">
					<div class="footer_column footer_contact">
						<div class="logo_container">
							<div class="logo"><a href="#"><img src="<?php print $logo; ?>" /></a></div>
						</div>
						<div class="footer_title">Got Question? Call Us 24/7</div>
						<div class="footer_phone"><?php print $contact_number; ?></div>
						<div class="footer_contact_text">
							<p><?php print $contact_address; ?></p>
						</div>
						<div class="footer_social">
							<ul>
							<?php
							if($facebook != ""):
								print "<li><a href=\"http://facebook.com/{$facebook}\"><i class=\"fab fa-facebook-f\"></i></a></li>";
							endif;
							if($twitter != ""):
								print" <li><a href=\"https://twitter.com/{$twitter}\"><i class=\"fab fa-twitter\"></i></a></li>";
							endif;
							if($instagram != ""):
								print" <li><a href=\"https://instagram.com/{$instagram}\"><i class=\"fab fa-instagram\"></i></a></li>";
							endif;
							?>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-lg-2 offset-lg-2">
					<div class="footer_column">
						<div class="footer_title">Find it Fast</div>
						<ul class="footer_list">
							<?php categoriesLinks(); ?>
						</ul>
					</div>
				</div>

				<div class="col-lg-2">
					<div class="footer_column">
						<div class="footer_title">Customer Care</div>
						<ul class="footer_list">
							<li><a href="#">My Account</a></li>
							<li><a href="#">Order Tracking</a></li>
							<li><a href="#">Customer Services</a></li>
							<li><a href="#">Returns / Exchange</a></li>
							<li><a href="#">FAQs</a></li>
							<li><a href="#">Product Support</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</footer>

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start">
						<div class="copyright_content">Copyright &copy; <?php print $company_name; ?>&nbsp;<script>document.write(new Date().getFullYear());</script>&nbsp;All rights reserved. Powered by <?php print $powered_by; ?></div>
						<div class="logos ml-sm-auto">
							<ul class="logos_list">
								<li><a href="#"><img src="images/logos_1.png" alt=""></a></li>
								<li><a href="#"><img src="images/logos_2.png" alt=""></a></li>
								<li><a href="#"><img src="images/logos_3.png" alt=""></a></li>
								<li><a href="#"><img src="images/logos_4.png" alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="js/jquery-3.3.1.min.js"></script><script src="styles/bootstrap4/popper.js"></script><script src="styles/bootstrap4/bootstrap.min.js"></script><script src="plugins/greensock/TweenMax.min.js"></script><script src="plugins/greensock/TimelineMax.min.js"></script><script src="plugins/scrollmagic/ScrollMagic.min.js"></script><script src="plugins/greensock/animation.gsap.min.js"></script><script src="plugins/greensock/ScrollToPlugin.min.js"></script><script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script><script src="plugins/slick-1.8.0/slick.js"></script><script src="plugins/easing/easing.js"></script><script src="js/custom.js"></script>
</body>

</html>
<?php
}
//End Website Parts

//Website (Business)
function singleItemsDisplay(){
	global $db_connect;
	$items = mysqli_query($db_connect,"SELECT * FROM products WHERE item_name = '{$_GET["n"]}'  ORDER BY id DESC LIMIT 1 ");
	while($it = mysqli_fetch_array($item)){
		$item_name				= $it["item_name"];
		$item_amount			= $it["item_amount"];
		$item_old_amount		= $it["item_old_amount"];
		$item_colors				= $it["item_colors"];
		$item_description		= $it["item_description"];
		$item_added_info		= $it["item_added_description"];
		$item_main_image		= $it["item_main_image"];
		$item_all_image			= $it["item_all_image"];
		$item_brand				= $it["item_brand"];
		$item_category			= $it["item_category"];
?>

<?php
	}
}

function shopItemsDisplay(){
	$db_connect = mysqli_connect("localhost","root","","ecommerce");
	$items = mysqli_query($db_connect,"SELECT * FROM products ORDER BY id DESC LIMIT 0,50 ");
	while($it = mysqli_fetch_array($items)){
		$item_name				= $it["item_name"];
		$item_amount			= $it["item_amount"];
		$item_old_amount		= $it["item_old_amount"];
		$item_main_image		= $it["item_main_image"];
		$item_brand				= $it["item_brand"];
		$item_category			= $it["item_category"];
?>
<div class="arrivals_slider_item">
<div class="product_item is_new d-flex flex-column align-items-center justify-content-center text-center">
<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="images/new_1.jpg" alt=""></div>
<div class="product_content">
<div class="product_price"><?php print $item_amount; ?></div>
<div class="product_name"><div><a href="product?<?php print $item_name; ?>"><?php print $item_name; ?></a></div></div>
<div class="product_extras">
<div class="product_color"><input type="radio" checked name="product_color" style="background:#b19c83"><input type="radio" name="product_color" style="background:#000000"><input type="radio" name="product_color" style="background:#999999"></div>
<button class="product_cart_button">Add to Cart</button>
</div>
</div>
<div class="product_fav"><i class="fas fa-heart"></i></div>
<ul class="product_marks">
<li class="product_mark product_discount">-<?php
if($item_old_amount == ""){
	print"";
} else{
	$calc1		= $item_amount/$item_old_amount;
	$calc2		= $calc1 * (100);
	$calc3		= 100 - $calc2;
	$discount	= round($calc3, 2);
	print $discount."%";
}
?></li>
<li class="product_mark product_new">new</li>
</ul>
</div>
</div>
<?php
	}
}

function categoriesItems(){
	global $db_connect;
	$items = mysqli_query($db_connect,"SELECT * FROM products WHERE item_category = {$_GET['c']} ORDER BY id DESC LIMIT 0,50 ");
	while($it = mysqli_fetch_array($item)){
		$item_name				= $it["item_name"];
		$item_amount			= $it["item_amount"];
		$item_old_amount		= $it["item_old_amount"];
		$item_main_image		= $it["item_main_image"];
		$item_brand				= $it["item_brand"];
		$item_category			= $it["item_category"];
?>

<?php
	}
}
function relatedItems(){
	global $db_connect;
	$items = mysqli_query($db_connect,"SELECT * FROM products WHERE item_category = {$_GET['c']} ORDER BY id DESC LIMIT 0,50 ");
	while($it = mysqli_fetch_array($item)){
		$item_name				= $it["item_name"];
		$item_amount			= $it["item_amount"];
		$item_old_amount		= $it["item_old_amount"];
		$item_main_image		= $it["item_main_image"];
		$item_brand				= $it["item_brand"];
		$item_category			= $it["item_category"];
?>

<?php
	}
}

function categoriesLinks(){
	//global $db_connect;
	$db_connect = mysqli_connect("localhost","root","","ecommerce");
	$category = mysqli_query($db_connect,"SELECT * FROM categories");
	while($cat = mysqli_fetch_array($category)){
		$url	= $cat["cat_name"];
		$link	= $cat["cat_link"];
?>
<li><a href="category?d=<?php print $link; ?>"><?php print $url; ?><i class="fas fa-chevron-right"></i></a></li>
<?php
	}
}

mysqli_close($db_connect);
?>