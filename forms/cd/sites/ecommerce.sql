-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2020 at 07:36 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- CREATE DATABASE IF NOT EXISTS `ecommerce` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
-- USE `ecommerce`;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(100) NOT NULL,
  `cat_name` text NOT NULL,
  `cat_description` text NOT NULL,
  `cat_image` text NOT NULL,
  `cat_link` text NOT NULL,
  `cat_added_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `cat_description`, `cat_image`, `cat_link`, `cat_added_by`) VALUES
(1, 'Computers & Laptops ', '', '', 'computer-laptops', 'ceo'),
(2, 'Smartphones & Tablets', '', '', 'smartphones-tablets', 'ceo'),
(3, 'TV & Audio', '', '', 'tv-audio', 'ceo'),
(4, 'Gadgets', '', '', 'gadgets', 'ceo'),
(5, 'Car Electronics', '', '', 'cars-electronics', 'ceo'),
(6, 'Video Games & Consoles', '', '', 'videogames-consoles', 'ceo'),
(7, 'Accessories', '', '', 'accessories', 'ceo');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(100) NOT NULL,
  `item_name` text NOT NULL,
  `item_description` longtext NOT NULL,
  `item_added_description` longtext NOT NULL,
  `item_colors` text NOT NULL,
  `item_main_image` text NOT NULL,
  `item_images` longtext NOT NULL,
  `item_amount` text NOT NULL,
  `item_old_amount` text NOT NULL,
  `item_brand` text NOT NULL,
  `item_category` text NOT NULL,
  `item_discount_rate` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `item_name`, `item_description`, `item_added_description`, `item_colors`, `item_main_image`, `item_images`, `item_amount`, `item_old_amount`, `item_brand`, `item_category`, `item_discount_rate`) VALUES
(1, 'Tecno Phantom 9', 'Beautify your Life with the Latest Tecno Phantom.\r\nAmazing Back camera...', '', 'neon blue', 'phantom9.jpg', 'phantom9.jpg,phantom9-1.jpg,phantom9-2.jpg', '79000', '85000', 'Tecno', 'smartphone-tablets', '30'),
(2, 'Tecno Phantom 10', 'Beautify your Life with the Latest Tecno Phantom.\r\nAmazing Back camera...', '', 'neon blue', 'phantom9.jpg', 'phantom9.jpg,phantom9-1.jpg,phantom9-2.jpg', '79000', '95000', 'Tecno', 'smartphone-tablets', '30'),
(3, 'Tecno Phantom 11', 'Beautify your Life with the Latest Tecno Phantom.\r\nAmazing Back camera...', '', 'neon blue', 'phantom9.jpg', 'phantom9.jpg,phantom9-1.jpg,phantom9-2.jpg', '79000', '105000', 'Tecno', 'smartphone-tablets', '30');

-- --------------------------------------------------------

--
-- Table structure for table `web_settings`
--

CREATE TABLE `web_settings` (
  `id` int(100) NOT NULL,
  `company_name` text NOT NULL,
  `company_logo` longtext NOT NULL,
  `company_number` text NOT NULL,
  `company_email` text NOT NULL,
  `open_hours` text NOT NULL,
  `theme_color` text NOT NULL,
  `company_currency` text NOT NULL,
  `contact_address` text NOT NULL,
  `about_us` text NOT NULL,
  `partners` text NOT NULL,
  `contact_page_image` text NOT NULL,
  `social_instagram` text NOT NULL,
  `social_facebook` text NOT NULL,
  `social_twitter` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_settings`
--

INSERT INTO `web_settings` (`id`, `company_name`, `company_logo`, `company_number`, `company_email`, `open_hours`, `theme_color`, `company_currency`, `contact_address`, `about_us`, `partners`, `contact_page_image`, `social_instagram`, `social_facebook`, `social_twitter`) VALUES
(1, 'Business', 'logos/index.png', '+234-8100000000', 'company@mail.ocoms', '9am-6am', 'blue', 'N', 'Nigeria', 'We are an E-commerce website', 'Ocoms, This is me...', 'contact-page.jpg', 'business', 'business', 'business');

-- --------------------------------------------------------

--
-- Table structure for table `web_users`
--

CREATE TABLE `web_users` (
  `id` int(100) NOT NULL,
  `user_name` text NOT NULL,
  `user_email` text NOT NULL,
  `user_phone` text NOT NULL,
  `user_address` text NOT NULL,
  `user_password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_settings`
--
ALTER TABLE `web_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `web_settings`
--
ALTER TABLE `web_settings`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
