<?php
#Hi from Ocoms Developers.

#This script  was designed, debugged and administered by Ocoms.
#This script is licensed by, and remains a trademark of Ocoms.

#If errors occur please do contact Ocoms Developers via mail @" hi.developers@myocoms.com ".
#If you are using .NET Server please do contact Ocoms Developers via mail @" hi.developers@myocoms.com " as quick as possible.

#© Ocoms LLC. https://myocoms.com.

#Nothing should be editted under this Line as we have made sure no errors should be encountered.
#If errors should arise due to unauthorized editting, service charges will arise.

#error_reporting(0);

//Database
$db = mysqli_connect("localhost","root","","tekky_ecommerce_site");

//General Website Info
$web_data = mysqli_query($db,"SELECT * FROM web_settings");
	while($d = mysqli_fetch_array($web_data)){
		$company_name	= $d["company_name"];
		$logo					=	$d["company_logo"];
		$contact_number	= $d["company_number"];
		$contact_email		= $d["company_email"];
		$open_hours			= $d["open_hours"];
		$theme_color		= $d["theme_color"];
		$currency				= $d["company_currency"];
		$contact_address	= $d["contact_address"];
		$about_us				= $d["about_us"];
		$partners				=	$d["partners"];
		$footer_about_us	=  substr($about_us,0,100);
		$contactPageImage	=	$d["contact_page_image"];
		$instagram				=	$d["social_instagram"];
		$facebook				=	$d["social_facebook"];
		$twitter					=	$d["social_twitter"];
		$powered_by		= "<a href=\"https://myocoms.com\">Ocoms LLC.</a>";
		$year					= date("Y");
	}
//End of General Info

//Admin
function adminEditSettings(){
	global $db;
	$updated = mysqli_query($db,"UPDATE web_settings SET

	");
	if($updated){
		$response = "Updated Successfully!";
	}
	return $response;
}

function adminAddCategory(){
	global $db;
	
	$added = mysqli_query($db,"INSERT INTO categories SET
		cat_name = '{}',
		cat_description = '{}',
		cat_image = '{}',
		cat_link = '{}',
		cat_added_by = '{}'
	");
	if($added){
		$response = "Added Successfully!";
	}
	return $response;
}

function adminEditCategory(){
	global $db;
	$updated = mysqli_query($db,"UPDATE categories SET
		cat_name = '{}',
		cat_description = '{}',
		cat_image = '{}',
		cat_link = '{}',
		cat_added_by = '{}'
	");
	if($updated){
		$response = "Updated Successfully!";
	}
	return $response;
}

function adminDeleteCategory($catId){
	global $db;
	$deleted = mysqli_query($db,"DELETE * FROM categories WHERE id = '{$catId}' ");
	if($deleted){
		$response = "Deleted Successfully!";
	}
	return $response;
}

function adminAddProduct(){
	global $db;
	
	$added = mysqli_query($db,"INSERT INTO products SET
		item_name = '{}',
		item_description = '{}',
		item_added_description = '{}',
		item_colors = '{}',
		item_main_image = '{}',
		item_images = '{}',
		item_amount = '{}',
		item_old_amount = '{}',
		item_brand = '{}',
		item_category = '{}',
		item_discount_rate = '{}'
	");
	if($added){
		$response = "Added Successfully!";
	}
	return $response;
}

function adminEditProduct($proId){
	global $db;
	
	$updated = mysqli_query($db,"UPDATE products SET
		item_name = '{}',
		item_description = '{}',
		item_added_description = '{}',
		item_colors = '{}',
		item_main_image = '{}',
		item_images = '{}',
		item_amount = '{}',
		item_old_amount = '{}',
		item_brand = '{}',
		item_category = '{}',
		item_discount_rate = '{}'
	WHERE id = '{$proId}' ");
	if($updated){
		$response = "Updated Successfully!";
	}
	return $response;
}

function adminDeleteProduct($productId){
	global $db;
	$deleted = mysqli_query($db,"DELETE * FROM products WHERE id = '{$productId}' ");
	if($deleted){
		$response = "Deleted Successfully!";
	}
	return $response;
}
//End of Admin
?>