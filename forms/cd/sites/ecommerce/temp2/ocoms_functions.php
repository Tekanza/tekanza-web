<?php
#Hi from Emfati Developers.

#This script  was designed, debugged and administered by Emfati.
#This script is licensed by, and remains a trademark of Emfati.

#If errors occur please do contact Emfati Developers via mail @" hi.developers@emfati.com ".
#If you are using .NET Server please do contact Emfati Developers via mail @" hi.developers@emfati.com " as quick as possible.

#© Emfati LLC. https://emfati.com.

#Nothing should be editted under this Line as we have made sure no errors should be encountered.
#If errors should arise due to unauthorized editting, service charges will arise.

#error_reporting(0);

//Database
$db_connect = mysqli_connect("localhost","root","","myWeb");

//General Website Info
$web_data = mysqli_query($db_connect,"SELECT * FROM web_settings");
while($d = mysqli_fetch_array($web_data)){
	$company_name	= $d["company_name"];
	$logo					=	$d["company_logo"];
	$contact_number	= $d["company_number"];
	$contact_email		= $d["company_email"];
	$open_hours			= $d["open_hours"];
	$theme_color		= $d["theme_color"];
	$currency				= $d["company_currency"];
	$contact_address	= $d["contact_address"];
	$about_us				= $d["about_us"];
	$partners				=	$d["partners"];
	$footer_about_us	=  substr($about_us,0,100);
	$contactPageImage	=	$d["contact_page_image"];
	$instagram				=	$d["social_instagram"];
	$facebook				=	$d["social_facebook"];
	$twitter					=	$d["social_twitter"];
	$powered_by		= "<a href=\"https://emfati.com\">Emfati LLC.</a>";
	$year					= date("Y");
}
//End of General Info

//Website Parts
function webCss(){
	
}
function webJs(){
	
}

function webHeader(){
	global $logo, $contact_number, $contact_email, $open_hours, $instagram, $facebook, $twitter;
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<base href="" />
<link rel="shortcut icon" href="./images/<?php print $logo; ?>" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="plugins/slick-1.8.0/slick.css">
<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="styles/responsive.css">

</head>

<body>

<div class="super_container">
	<header class="header">
		<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="top_bar_contact_item"><div class="top_bar_icon"><img src="images/phone.png" alt=""></div><a href="tel:<?php print $contact_number; ?>"><?php print $contact_number; ?></a></div>
						<div class="top_bar_contact_item"><div class="top_bar_icon"><img src="images/mail.png" alt=""></div><a href="#"><?php print $contact_email; ?></a></div>
						<div class="top_bar_content ml-auto">
								<div class="top_bar_user">
								<div class="user_icon"><img src="images/user.svg" alt=""></div>
								<?php
								if(empty($_SESSION["biz_user"])){
								?>
								<div><a href="signin#register">Register</a></div>
								<div><a href="signin">Sign in</a></div>
								<?php
								}else{
									print "Username";
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>		
		</div>
		<div class="header_main">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 col-sm-3 col-3 order-1">
						<div class="logo_container">
							<div class="logo"><a href=""><img src="./images/logos/<?php print $logo; ?>" /></a></div>
						</div>
					</div>
					<div class="col-lg-6 col-12 order-lg-2 order-3 text-lg-left text-right">
						<div class="header_search">
							<div class="header_search_content">
								<div class="header_search_form_container">
									<form action="" class="header_search_form clearfix">
										<input type="search" required="required" class="header_search_input" placeholder="Search for products...">
										<div class="custom_dropdown">
											<div class="custom_dropdown_list">
												<span class="custom_dropdown_placeholder clc">All Categories</span>
												<i class="fas fa-chevron-down"></i>
												<ul class="custom_list clc">
													<li><a class="clc" href="#">All Categories</a></li>
													<li><a class="clc" href="#">Computers</a></li>
													<li><a class="clc" href="#">Laptops</a></li>
													<li><a class="clc" href="#">Cameras</a></li>
													<li><a class="clc" href="#">Hardware</a></li>
													<li><a class="clc" href="#">Smartphones</a></li>
												</ul>
											</div>
										</div>
										<button type="submit" class="header_search_button trans_300" value="Submit"><img src="images/search.png" alt=""></button>
									</form>
								</div>
							</div>
						</div>
					</div>

						<?php cart(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Main Navigation -->

		<nav class="main_nav">
			<div class="container">
				<div class="row">
					<div class="col">
						
						<div class="main_nav_content d-flex flex-row">
							<div class="cat_menu_container">
								<div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
									<div class="cat_burger"><span></span><span></span><span></span></div>
									<div class="cat_menu_text">categories</div>
								</div>

								<ul class="cat_menu">
									<?php categoriesLinks(); ?>
								</ul>
							</div>
							<div class="main_nav_menu ml-auto">
								<ul class="standard_dropdown main_nav_dropdown">
									<li><a href="#">Home<i class="fas fa-chevron-down"></i></a></li>
									<li class="hassubs">
										<a href="#">Super Deals<i class="fas fa-chevron-down"></i></a>
										<ul>
											<li>
												<a href="#">Menu Item<i class="fas fa-chevron-down"></i></a>
												<ul>
													<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
													<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
													<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
												</ul>
											</li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
										</ul>
									</li>
									<li class="hassubs">
										<a href="#">Featured Brands<i class="fas fa-chevron-down"></i></a>
										<ul>
											<li>
												<a href="#">Menu Item<i class="fas fa-chevron-down"></i></a>
												<ul>
													<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
													<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
													<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
												</ul>
											</li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
										</ul>
									</li>
									<li class="hassubs">
										<a href="#">Pages<i class="fas fa-chevron-down"></i></a>
										<ul>
											<li><a href="shop.html">Shop<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="product.html">Product<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="blog.html">Blog<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="blog_single.html">Blog Post<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="regular.html">Regular Post<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="cart.html">Cart<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="contact.html">Contact<i class="fas fa-chevron-down"></i></a></li>
										</ul>
									</li>
									<li><a href="blog.html">Blog<i class="fas fa-chevron-down"></i></a></li>
									<li><a href="contact.html">Contact<i class="fas fa-chevron-down"></i></a></li>
								</ul>
							</div>

							<!-- Menu Trigger -->

							<div class="menu_trigger_container ml-auto">
								<div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
									<div class="menu_burger">
										<div class="menu_trigger_text">menu</div>
										<div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</nav>
		
		<!-- Menu -->

		<div class="page_menu">
			<div class="container">
				<div class="row">
					<div class="col">
						
						<div class="page_menu_content">
							
							<div class="page_menu_search">
								<form action="">
									<input type="search" required="required" name="s" class="page_menu_search_input" placeholder="Search for products...">
								</form>
							</div>
							<ul class="page_menu_nav">
								<li class="page_menu_item">
									<a href="#">Home<i class="fa fa-angle-down"></i></a>
								</li>
								<li class="page_menu_item has-children">
									<a href="#">Super Deals<i class="fa fa-angle-down"></i></a>
									<ul class="page_menu_selection">
										<li><a href="#">Super Deals<i class="fa fa-angle-down"></i></a></li>
										<li class="page_menu_item has-children">
											<a href="#">Menu Item<i class="fa fa-angle-down"></i></a>
											<ul class="page_menu_selection">
												<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
												<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
												<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
												<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
											</ul>
										</li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
									</ul>
								</li>
								<li class="page_menu_item has-children">
									<a href="#">Featured Brands<i class="fa fa-angle-down"></i></a>
									<ul class="page_menu_selection">
										<li><a href="#">Featured Brands<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
									</ul>
								</li>
								<li class="page_menu_item has-children">
									<a href="#">Trending Styles<i class="fa fa-angle-down"></i></a>
									<ul class="page_menu_selection">
										<li><a href="#">Trending Styles<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
									</ul>
								</li>
								<li class="page_menu_item"><a href="blog.html">blog<i class="fa fa-angle-down"></i></a></li>
								<li class="page_menu_item"><a href="contact.html">contact<i class="fa fa-angle-down"></i></a></li>
							</ul>
							
							<div class="menu_contact">
								<div class="menu_contact_item"><div class="menu_contact_icon"><img src="images/phone_white.png" alt=""></div><a href="tel:<?php print $contact_number; ?>"><?php print $contact_number; ?></a></div>
								<div class="menu_contact_item"><div class="menu_contact_icon"><img src="images/mail_white.png" alt=""></div><a href="#"><?php print $contact_email; ?></a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</header>
<?php
}

function webFooter(){
	global $logo;
	global $company_name;
	global $contact_number;
	global $contact_email;
	global $contact_address;
	global $open_hours;
	global $footer_about_us;
	global $instagram;
	global $facebook;
	global $twitter;
	global $powered_by;
?>
	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center">
						<div class="newsletter_title_container">
							<div class="newsletter_icon"><img src="images/send.png" alt=""></div>
							<div class="newsletter_title">Sign up for Newsletter</div>
							<div class="newsletter_text"><p>...and receive %20 discount for first shopping.</p></div>
						</div>
						<div class="newsletter_content clearfix">
							<form action="#" class="newsletter_form">
								<input type="email" class="newsletter_input" required="required" placeholder="Enter your email address">
								<button class="newsletter_button">Subscribe</button>
							</form>
							<div class="newsletter_unsubscribe_link"><a href="#">unsubscribe</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 footer_col">
					<div class="footer_column footer_contact">
						<div class="logo_container">
							<div class="logo"><a href="#"><img src="<?php print $logo; ?>" /></a></div>
						</div>
						<div class="footer_title">Got Question? Call Us 24/7</div>
						<div class="footer_phone"><?php print $contact_number; ?></div>
						<div class="footer_contact_text">
							<p><?php print $contact_address; ?></p>
						</div>
						<div class="footer_social">
							<ul>
							<?php
							if($facebook != ""):
								print "<li><a href=\"http://facebook.com/{$facebook}\"><i class=\"fab fa-facebook-f\"></i></a></li>";
							endif;
							if($twitter != ""):
								print" <li><a href=\"https://twitter.com/{$twitter}\"><i class=\"fab fa-twitter\"></i></a></li>";
							endif;
							if($instagram != ""):
								print" <li><a href=\"https://instagram.com/{$instagram}\"><i class=\"fab fa-instagram\"></i></a></li>";
							endif;
							?>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-lg-2 offset-lg-2">
					<div class="footer_column">
						<div class="footer_title">Find it Fast</div>
						<ul class="footer_list">
							<?php categoriesLinks(); ?>
						</ul>
					</div>
				</div>

				<div class="col-lg-2">
					<div class="footer_column">
						<div class="footer_title">Customer Care</div>
						<ul class="footer_list">
							<li><a href="#">My Account</a></li>
							<li><a href="#">Order Tracking</a></li>
							<li><a href="#">Customer Services</a></li>
							<li><a href="#">Returns / Exchange</a></li>
							<li><a href="#">FAQs</a></li>
							<li><a href="#">Product Support</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</footer>

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start">
						<div class="copyright_content">Copyright &copy; <?php print $company_name; ?>&nbsp;<script>document.write(new Date().getFullYear());</script>&nbsp;All rights reserved. Powered by <?php print $powered_by; ?></div>
						<div class="logos ml-sm-auto">
							<ul class="logos_list">
								<li><a href="#"><img src="images/logos_1.png" alt=""></a></li>
								<li><a href="#"><img src="images/logos_2.png" alt=""></a></li>
								<li><a href="#"><img src="images/logos_3.png" alt=""></a></li>
								<li><a href="#"><img src="images/logos_4.png" alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="js/jquery-3.3.1.min.js"></script><script src="styles/bootstrap4/popper.js"></script><script src="styles/bootstrap4/bootstrap.min.js"></script><script src="plugins/greensock/TweenMax.min.js"></script><script src="plugins/greensock/TimelineMax.min.js"></script><script src="plugins/scrollmagic/ScrollMagic.min.js"></script><script src="plugins/greensock/animation.gsap.min.js"></script><script src="plugins/greensock/ScrollToPlugin.min.js"></script><script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script><script src="plugins/slick-1.8.0/slick.js"></script><script src="plugins/easing/easing.js"></script><script src="js/custom.js"></script>
</body>

</html>
<?php
}
//End Website Parts

//Admin
function adminEditSettings(){
	global $db_connect;
	$updated = mysqli_query($db_connect,"UPDATE web_settings SET

	");
	if($updated){
		$response = "Updated Successfully!";
	}
	return $response;
}

function adminAddCategory(){
	global $db_connect;
	
	$added = mysqli_query($db_connect,"INSERT INTO categories SET
		cat_name = '{}',
		cat_description = '{}',
		cat_image = '{}',
		cat_link = '{}',
		cat_added_by = '{}'
	");
	if($added){
		$response = "Added Successfully!";
	}
	return $response;
}

function adminEditCategory(){
	global $db_connect;
	$updated = mysqli_query($db_connect,"UPDATE categories SET
		cat_name = '{}',
		cat_description = '{}',
		cat_image = '{}',
		cat_link = '{}',
		cat_added_by = '{}'
	");
	if($updated){
		$response = "Updated Successfully!";
	}
	return $response;
}

function adminDeleteCategory($catId){
	global $db_connect;
	$deleted = mysqli_query($db_connect,"DELETE * FROM categories WHERE id = '{$catId}' ");
	if($deleted){
		$response = "Deleted Successfully!";
	}
	return $response;
}

function adminAddProduct(){
	global $db_connect;
	
	$added = mysqli_query($db_connect,"INSERT INTO products SET
		item_name = '{}',
		item_description = '{}',
		item_added_description = '{}',
		item_colors = '{}',
		item_main_image = '{}',
		item_images = '{}',
		item_amount = '{}',
		item_old_amount = '{}',
		item_brand = '{}',
		item_category = '{}',
		item_discount_rate = '{}'
	");
	if($added){
		$response = "Added Successfully!";
	}
	return $response;
}

function adminEditProduct($proId){
	global $db_connect;
	
	$updated = mysqli_query($db_connect,"UPDATE products SET
		item_name = '{}',
		item_description = '{}',
		item_added_description = '{}',
		item_colors = '{}',
		item_main_image = '{}',
		item_images = '{}',
		item_amount = '{}',
		item_old_amount = '{}',
		item_brand = '{}',
		item_category = '{}',
		item_discount_rate = '{}'
	WHERE id = '{$proId}' ");
	if($updated){
		$response = "Updated Successfully!";
	}
	return $response;
}

function adminDeleteProduct($productId){
	global $db_connect;
	$deleted = mysqli_query($db_connect,"DELETE * FROM products WHERE id = '{$productId}' ");
	if($deleted){
		$response = "Deleted Successfully!";
	}
	return $response;
}
//End of Admin

//Website (Business)
function singleItemsDisplay(){
	global $db_connect;
	$items = mysqli_query($db_connect,"SELECT * FROM products WHERE item_name = '{$_GET["n"]}'  ORDER BY id DESC LIMIT 1 ");
	while($it = mysqli_fetch_array($item)){
		$item_name				= $it["item_name"];
		$item_amount			= $it["item_amount"];
		$item_old_amount		= $it["item_old_amount"];
		$item_colors				= $it["item_colors"];
		$item_description		= $it["item_description"];
		$item_added_info		= $it["item_added_description"];
		$item_main_image		= $it["item_main_image"];
		$item_all_image			= $it["item_all_image"];
		$item_brand				= $it["item_brand"];
		$item_category			= $it["item_category"];
?>

<?php
	}
}

function shopItemsDisplay(){
	$db_connect = mysqli_connect("localhost","root","","ecommerce");
	$items = mysqli_query($db_connect,"SELECT * FROM products ORDER BY id DESC LIMIT 0,50 ");
	while($it = mysqli_fetch_array($items)){
		$item_name				= $it["item_name"];
		$item_amount			= $it["item_amount"];
		$item_old_amount		= $it["item_old_amount"];
		$item_main_image		= $it["item_main_image"];
		$item_brand				= $it["item_brand"];
		$item_category			= $it["item_category"];
?>
<div class="arrivals_slider_item">
<div class="product_item is_new d-flex flex-column align-items-center justify-content-center text-center">
<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="images/new_1.jpg" alt=""></div>
<div class="product_content">
<div class="product_price"><?php print $item_amount; ?></div>
<div class="product_name"><div><a href="product?<?php print $item_name; ?>"><?php print $item_name; ?></a></div></div>
<div class="product_extras">
<div class="product_color"><input type="radio" checked name="product_color" style="background:#b19c83"><input type="radio" name="product_color" style="background:#000000"><input type="radio" name="product_color" style="background:#999999"></div>
<button class="product_cart_button">Add to Cart</button>
</div>
</div>
<div class="product_fav"><i class="fas fa-heart"></i></div>
<ul class="product_marks">
<li class="product_mark product_discount">-<?php
if($item_old_amount == ""){
	print"";
} else{
	$calc1		= $item_amount/$item_old_amount;
	$calc2		= $calc1 * (100);
	$calc3		= 100 - $calc2;
	$discount	= round($calc3, 2);
	print $discount."%";
}
?></li>
<li class="product_mark product_new">new</li>
</ul>
</div>
</div>
<?php
	}
}

function categoriesItems(){
	global $db_connect;
	$items = mysqli_query($db_connect,"SELECT * FROM products WHERE item_category = {$_GET['c']} ORDER BY id DESC LIMIT 0,50 ");
	while($it = mysqli_fetch_array($item)){
		$item_name				= $it["item_name"];
		$item_amount			= $it["item_amount"];
		$item_old_amount		= $it["item_old_amount"];
		$item_main_image		= $it["item_main_image"];
		$item_brand				= $it["item_brand"];
		$item_category			= $it["item_category"];
?>

<?php
	}
}
function relatedItems(){
	global $db_connect;
	$items = mysqli_query($db_connect,"SELECT * FROM products WHERE item_category = {$_GET['c']} ORDER BY id DESC LIMIT 0,50 ");
	while($it = mysqli_fetch_array($item)){
		$item_name				= $it["item_name"];
		$item_amount			= $it["item_amount"];
		$item_old_amount		= $it["item_old_amount"];
		$item_main_image		= $it["item_main_image"];
		$item_brand				= $it["item_brand"];
		$item_category			= $it["item_category"];
?>

<?php
	}
}

function categoriesLinks(){
	//global $db_connect;
	$db_connect = mysqli_connect("localhost","root","","ecommerce");
	$category = mysqli_query($db_connect,"SELECT * FROM categories");
	while($cat = mysqli_fetch_array($category)){
		$url	= $cat["cat_name"];
		$link	= $cat["cat_link"];
?>
<li><a href="category?d=<?php print $link; ?>"><?php print $url; ?><i class="fas fa-chevron-right"></i></a></li>
<?php
	}
}

function addtoCart(){
	global $db_connect;
	
}

function cart(){
?>
					<div class="col-lg-4 col-9 order-lg-3 order-2 text-lg-left text-right">
						<div class="wishlist_cart d-flex flex-row align-items-center justify-content-end">
							<div class="wishlist d-flex flex-row align-items-center justify-content-end">
								<div class="wishlist_icon"><img src="images/heart.png" alt=""></div>
								<div class="wishlist_content">
									<div class="wishlist_text"><a href="#">Wishlist</a></div>
									<div class="wishlist_count">115</div>
								</div>
							</div>
							<div class="cart">
								<div class="cart_container d-flex flex-row align-items-center justify-content-end">
									<div class="cart_icon">
										<img src="images/cart.png" alt="">
										<div class="cart_count"><span>10</span></div>
									</div>
									<div class="cart_content">
										<div class="cart_text"><a href="#">Cart</a></div>
										<div class="cart_price">$85</div>
									</div>
								</div>
							</div>
<?php
}

mysqli_close($db_connect);
?>