<!doctype html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><meta name="referrer" content="no-referrer" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="referrer" content="no-referrer" /><meta name="robots" content="noindex,nofollow" />
<title>MyOcoms Ember offer with Tekanza</title>
<link rel="stylesheet" href="css/main2.css?v=<?= time(); ?>" /><link rel="stylesheet" href="css/bootstrap.min.css" />
<style>body{width:100%;}.toppy{padding:2px 15px;}.header{width:100%;padding:15px 5px;background:#444;color:#fff;position:sticky;top:0;z-index:300;}.info{border:2.5px solid #bee5eb;}.right{text-align:right;}</style>
</head>
<body>
<div class="toppy"><a href="mailto:info@tekanza.com">info@tekanza.com</a>|<a href="mailto:hi@myocoms.com">hi@myocoms.com</a>|<small><b>Support:</b> - </small><a href="tel:+234-9060976211">+234-9060976211</a><div class="right"><a href="files?url=terms-and-conditions"><u>Terms and Conditions</u></a>&nbsp;|&nbsp;<a href="files?url=privacy"><u>Privacy</u></a></div></div>
<div class="header">
    <img style="width:150px;height:22px;" src="../images/tek_3.png" />
</div>
<div class="container alert info">
<h1>Request Form</h1>
<div class="res"></div>
    <marquee><b>This offer is for Nigerians only!</b></marquee>
</div>
<div class="container alert info">
<form class="form re n sse" id="form">
<fieldset>
<legend>Personal Info</legend>
<div class="row">
	<div class="col-md-6">
	<label>Full name <sub style="color:#900; font-size:1.5em;"><strong>*</strong></sub></label>
		<input class="form-control" type="text" name="fieldinput[pInfo][0]" placeholder="Full name (Surname first)" required />
	</div>
	<div class="col-md-6">
	<label>Email address <sub style="color:#900; font-size:1.5em;"><strong>*</strong></sub></label>
		<input class="form-control" type="email" name="fieldinput[pInfo][1]" required id="mail" />
	</div>
</div>
<div class="row">
	<div class="col-md-6">
	<label>Phone number <sub style="color:#900; font-size:1.5em;"><strong>*</strong></sub></label>
		<input class="form-control" type="tel" name="fieldinput[pInfo][2]" id="phone" required />
	</div>
	<div class="col-md-6">
	<label>Personal address <sub style="color:#900; font-size:1.5em;"><strong>*</strong></sub></label>
		<input class="form-control" type="text" id="mail" name="fieldinput[pInfo][3]" placeholder="No. 123 Nigeria, Nigeria" required />
	</div>
</div>
<div class="row">
	<div class="col-md-6">
	<label>Country <sub style="color:#900; font-size:1.5em;"><strong>*</strong></sub></label>
		<input class="form-control" type="text" name="fieldinput[pInfo][4]" value="Nigeria" readonly />
	</div>
	<div class="col-md-6">
	<label>State <sub style="color:#900; font-size:1.5em;"><strong>*</strong></sub></label>
		<select class="form-control" name="fieldinput[pInfo][5]">
			<option disabled value="null" selected></option>
			<option value="abia">Abia</option>
			<option value="adamawa">Adamawa</option>
			<option value="akwa-ibom">Akwa Ibom</option>
			<option value="anambra">Anambra</option>
			<option value="bauchi">Bauchi</option>
			<option value="bayelsa">Bayelsa</option>
			<option value="benue">Benue</option>
			<option value="borno">Bornu</option>
			<option value="cross-river">Cross River</option>
			<option value="delta">Delta</option>
			<option value="enbonyi">Ebonyi</option>
			<option value="edo">Edo</option>
			<option value="ekiti">Ekiti</option>
			<option value="enugu">Enugu</option>
			<option value="fct">FCT</option>
			<option value="gombe">Gombe</option>
			<option value="imo">Imo</option>
			<option value="jigawa">Jigawa</option>
			<option value="kaduna">Kaduna</option>
			<option value="kano">Kano</option>
			<option value="katsina">Katsina</option>
			<option value="kebbi">Kebbi</option>
			<option value="kogi">Kogi</option>
			<option value="kwara">Kwara</option>
			<option value="lagos">Lagos</option>
			<option value="niger">Niger</option>
			<option value="ogun">Ogun</option>
			<option value="ondo">Ondo</option>
			<option value="osun">Osun</option>
			<option value="oyo">Oyo</option>
			<option value="plateau">Plateau</option>
			<option value="rivers">Rivers</option>
			<option value="sokoto">Sokoto</option>
			<option value="taraba">Taraba</option>
			<option value="yobe">Yobe</option>
			<option value="zamfara">Zamfara</option>
		</select>
	</div>
</div>
</fieldset>

<hr />
<fieldset>
<legend>Request Info</legend>
<div class="row">
	<div class="col-md-6">
	<label>Type of website <sub style="color:#900; font-size:1.5em;"><strong>*</strong></sub></label>
		<select class="form-control tow" name="fieldinput[rInfo][0]" required>
		<option value="null" selected disabled></option>
		<option value="personal" data-id="bpw" disabled>Personal website (Coming up soon)</option>
		<option value="business" data-id="bw" disabled>Business website (Coming up soon)</option>
		<option value="ecommerce" data-id="ew">E-commerce</option>
		</select>
	</div>
</div>
<div class="row">
	<div class="col-md-12"><div id="dv0">
	<div class="row alert">
		<div class="col-md-3 temp1">
			<div class="card">
				<div>
					<img class="card-img-top" data-card-img="images/p-bg-title-001.jpg" data-card-text="Show your professionalism with this awesome template.<ul><p><b>Features</b></p><li>Multi page website</li><li>Includes: Home, About and Contact pages (Basic)</li></ul><br><p> Powered by <b>Colorlib</b></p>" src="images/p-bg-title-001.jpg" style="height:180px;" />
				</div>
				<div class="card-footer">
					<label class="switch"><span class="">Select template</span><input type="checkbox" class="st" value="Personal Website 1" name="fieldinput[rInfo][5]" /><span class="slider"></span></label>
				</div>
			</div>
		</div>
		<div class="col-md-3 temp1">
			<div class="card">
				<div>
					<img class="card-img-top" data-card-img="images/p-bg-title-002.jpg" data-card-text="Boost your site with this Single page template.<ul><p><b>Features</b></p><li>Single page website</li><li>Includes: Home, About and Contact pages (Basic)</li></ul><br><p> Powered by <b>Colorlib</b></p>" src="images/p-bg-title-002.jpg" style="height:180px;" />
				</div>
				<div class="card-footer">
					<label class="switch"><span class="">Select template</span><input type="checkbox" class="st" value="Personal Website 2" name="fieldinput[rInfo][5]" /><span class="slider"></span></label>
				</div>
			</div>
		</div>
		<div class="col-md-3 temp1">
			<div class="card">
				<div>
					<img class="card-img-top" data-card-img="images/p-bg-title-003.jpg" data-card-text="Show the world how marveling you can be with this well crafted template.<ul><p><b>Features</b></p><li>Multi page website</li><li>Includes: Home, About and Contact pages (Basic)</li></ul><br><p> Powered by <b>Colorlib</b></p>" src="images/p-bg-title-003.jpg" style="height:180px;" />
				</div>
				<div class="card-footer">
					<label class="switch"><span class="">Select template</span><input type="checkbox" class="st" value="Personal Website 3" name="fieldinput[rInfo][5]" /><span class="slider"></span></label>
				</div>
			</div>
		</div>
		
		
		<div class="col-md-3 temp2">
			<div class="card">
				<div>
					<img class="card-img-top" data-card-img="images/b-bg-title-001.jpg" data-card-text="Boost your website with this awesome template. Good for your business<ul><p><b>Features</b></p><li>Single page website</li><li>Includes: Home, About and Contact pages (Basic)</li></ul><br><p> Powered by <b>Colorlib</b></p>" src="images/b-bg-title-001.jpg" style="height:180px;" />
				</div>
				<div class="card-footer">
					<label class="switch"><span class="">Select template</span><input type="checkbox" class="st" value="Business Website 1" name="fieldinput[rInfo][5]" /><span class="slider"></span></label>
				</div>
			</div>
		</div>
		<div class="col-md-3 temp2">
			<div class="card">
				<div>
					<img class="card-img-top" data-card-img="images/b-bg-title-002.jpg" data-card-text="Feel complete with the ArcLab template arranged and prepared for any business structure just for you.<ul><p><b>Features</b></p><li>Multi page website</li><li>Includes: Home, About and Contact pages (Basic)</li></ul><br><p> Powered by <b>Colorlib</b></p>" src="images/b-bg-title-002.jpg" style="height:180px;" />
				</div>
				<div class="card-footer">
					<label class="switch"><span class="">Select template</span><input type="checkbox" class="st" value="Business Website 2" name="fieldinput[rInfo][5]" /><span class="slider"></span></label>
				</div>
			</div>
		</div>
		<div class="col-md-3 temp2">
			<div class="card">
				<div>
					<img class="card-img-top" data-card-img="images/b-bg-title-003.jpg" data-card-text="Show the world how marveling your business can be with this well crafted template.<ul><p><b>Features</b></p><li>Single page website</li><li>Includes: Home, About and Contact pages (Basic)</li></ul><br><p> Powered by <b>Colorlib</b></p>" src="images/b-bg-title-003.jpg" style="height:180px;" />
				</div>
				<div class="card-footer">
					<label class="switch"><span class="">Select template</span><input type="checkbox" class="st" value="Business Website 3" name="fieldinput[rInfo][5]" /><span class="slider"></span></label>
				</div>
			</div>
		</div>
		
		
		<div class="col-md-3 temp3">
			<div class="card">
				<div>
					<img class="card-img-top" data-card-img="images/e-bg-title-001.jpg" data-card-text="Hey!, are you having a cloth store or mall? then this might most definately suit your business structure.<ul><p><b>Features</b></p><li>Multi page website</li><li>Includes: Home, About, Payment gateway, New arrivals and Contact pages (Basic)</li></ul><br><p> Powered by <b>Colorlib</b></p>" src="images/e-bg-title-001.jpg" style="height:180px;" />
				</div>
				<div class="card-footer">
					<label class="switch"><span class="">Select template</span><input type="checkbox" class="st" value="Ecommerce Website 1" name="fieldinput[rInfo][5]" /><span class="slider"></span></label>
				</div>
			</div>
		</div>
		<div class="col-md-3 temp3">
			<div class="card">
				<div>
					<img class="card-img-top" data-card-img="images/e-bg-title-002.jpg" data-card-text="Hey!, are you owning/ managing a Gadget or Electronic store or mall? then this might most definately suit your business structure.<ul><p><b>Features</b></p><li>Multi page website</li><li>Includes: Home, About, Payment gateway, New arrivals and Contact pages (Basic)</li></ul><br><p> Powered by <b>Colorlib</b></p>" src="images/e-bg-title-002.jpg" style="height:180px;" />
				</div>
				<div class="card-footer">
					<label class="switch"><span class="">Select template</span><input type="checkbox" class="st" value="Ecommerce Website 2" name="fieldinput[rInfo][5]" /><span class="slider"></span></label>
				</div>
			</div>
		</div>
		<div class="col-md-3 temp3">
			<div class="card">
				<div>
					<img class="card-img-top" data-card-img="images/e-bg-title-003.jpg" data-card-text="Hey!, this is okay to be used by any store or mall.<ul><p><b>Features</b></p><li>Multi page website</li><li>Includes: Home, About, Payment gateway, New arrivals and Contact pages (Basic)</li></ul><br><p> Powered by <b>Colorlib</b></p>" src="images/e-bg-title-003.jpg" style="height:180px;" />
				</div>
				<div class="card-footer">
					<label class="switch"><span class="">Select template</span><input type="checkbox" class="st" value="Ecommerce Website 3" name="fieldinput[rInfo][5]" /><span class="slider"></span></label>
				</div>
			</div>
		</div>
		
	</div></div>
	<p><strong>Basic Pages: </strong>(Home, About & Contact<span class="bw-e">, Payment gateway, New arrivals, </span>)</p>
	<p><label class="switch"><span class="">Need and app for your site?</span><input type="checkbox" class="" value="Yes" name="fieldinput[rInfo][8]" /><span class="slider"></span></label></p>
	<p class=""><strong>Select pages in addition to the Basic Pages List:</strong></p>
	<div class="alert alert-info">
	<p class="bw-ew alert selection">Select not more than 4 : <span class="nnnn"> 0 selected</span></p>
	<div class="row" id="dv">
			<div class="col-md-3 bpw">
				<label class=""><span class="">Portfolio</span>&nbsp;&nbsp;<input type="checkbox" class="swt" value="portfolio" name="fieldinput[rInfo][1]" /></label>
			</div>
			<div class="col-md-3 bw-ew">
				<label class=""><span class="">Cart<br /><small>(Add to cart, view cart and checkout)</small></span>&nbsp;&nbsp;<input type="checkbox" class="swt" value="cart" name="fieldinput[rInfo][2]" /></label>
			</div>
			<div class="col-md-3 bw-ew">
				<label class=""><span class="">Product<br /><small>(Product categories)</small></span>&nbsp;&nbsp;<input type="checkbox" class="swt" value="product" name="fieldinput[rInfo][3]" /></label>
			</div>
			<div class="col-md-3 bpw">
				<label class=""><span class="">Services</span>&nbsp;&nbsp;<input type="checkbox" class="swt" value="services" name="fieldinput[rInfo][7]" /></label>
			</div>
			<div class="col-md-3 bw-ew">
				<label class=""><span class="">Shop</span>&nbsp;&nbsp;<input type="checkbox" class="swt" value="shop" name="fieldinput[rInfo][4]" /></label>
			</div>
		</div>
	</div>
	</div>
	<div class="col-md-6">
		<div class="alert alert-info">
			<div class="form-group">
				<label>Preferred domain name <small>:: tell us what URL you might love to use as your website's address<br />eg. : http://tekanza.com .</small><br />Only(.com and .com.ng) are allowed. No spaces in between url, only hyphens(-) and underscores(_) are allowed.<sub style="color:#900; font-size:1.5em;"><strong>*</strong></sub></label><input class="form-control" id="uri" placeholder="com-pany_na-me.com" data-domain="false" name="fieldinput[rInfo][6]" />
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="domain-validate"></div>
	</div>
</div>
</fieldset>

<hr />
<fieldset>
<legend>Business/ Company Info <small>(Not compulsory for personal websites.)</small></legend>
<div class="row">
	<div class="col-md-6">
	<label>Business/ Company name</label>
		<input class="form-control biz-info" type="text" name="fieldinput[bInfo][0]" />
	</div>
	<div class="col-md-6">
	<label>Business/ Company email</label>
		<input class="form-control biz-info" type="email" name="fieldinput[bInfo][1]" />
	</div>
</div>
<div class="row">
	<div class="col-md-6">
	<label>Business/ Company phone</label>
		<input class="form-control biz-info" type="phone" name="fieldinput[bInfo][2]" placeholder="" />
	</div>
	<div class="col-md-6">
	<label>Business/ Company address/ Personal address</label>
		<input class="form-control biz-info" type="text" name="fieldinput[bInfo][3]" placeholder="No. 123 Nigeria, Nigeria" />
	</div>
</div>
<div class="row">
	<div class="col-md-6">
	<label>Business/ Company category</label>
		<select class="form-control biz-info" name="fieldinput[bInfo][4]">
			<option value="null" disabled selected></option>
			<option value="cloths">Cloth store</option>
			<option value="electronics">Electronic store</option>
			<option value="fashion">Fashion</option>
			<option value="generalstore">General store/ Mall/ Vendor</option>
			<option value="groceries">Grocery store</option>
			<option value="ict">ICT company</option>
			<option value="manufacturing">Manufacturing company</option>
			<option value="others">Others</option>
		</select>
	</div>
	<div class="col-md-6">
	<label>RC/ BN <small>(Corporate Affairs Registered Number)</small></label>
		<div class="row"><div class="col-md-2"><label>RC <input type="radio" value="RC" name="fieldinput[bInfo][5][0]" /></label></div><div class="col-md-2"><label>BN <input type="radio" value="BN" name="fieldinput[bInfo][5][0]" /></label></div><div class="col-md-8"><input class="form-control biz-info" type="number" min="1" id="rc" placeholder="RC/ BN Number" oninput="javascript:if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="9" name="fieldinput[bInfo][5][1]" /></div></div>
	</div>
</div>
</fieldset>
<hr />
<fieldset>
<legend>Web Hosting Authentication</legend>
<div class="alert alert-warning">
<p><sub style="color:#900; font-size:.9em;"><strong>Please this is important!</strong></sub></p>
<p>You are required to create and submit a new email and password as it will be used for your web hosting authentication. Access to this mail can be restricted later after the completion of your website. Thank you...</p>
</div>
<div class="row">
	<div class="col-md-6">
	<label>Email<sub style="color:#900; font-size:1.5em;"><strong>*</strong></sub></label>
		<input class="form-control" type="email" name="fieldinput[cInfo][0]" required />
	</div>
	<div class="col-md-6">
	<label>Password<sub style="color:#900; font-size:1.5em;"><strong>*</strong></sub></label>
		<input class="form-control" type="text" name="fieldinput[cInfo][1]" required />
	</div>
</div>
</fieldset>


<hr />
<div class="alert info"><small><u>Note</u>: Please make sure you have read our <a href="files?url=terms-and-conditions">terms and conditions</a> before proceeding.</small><br />
<input type="checkbox" required value="agreed" id="agreed" name="fieldinput[accept][0]" /> <label for="agreed"><small>I have accepted the terms and conditions contained in this request.</small></label>
</div>
<button class="btn btn-secondary snd">Proceed</button>
</form>
</div>
<script src="./js/jquery.min.js"></script><script src="https://js.paystack.co/v1/inline.js"></script><script src="js/main2.js?v=<?= time(); ?>" type="text/javascript"></script>

</body>
</html>