<?php
session_start();
if(!$_SERVER['REQUEST_METHOD'] == 'POST' || !isset($_POST['r'])){  
	die("Transaction reference not found");
}
@$ref = $_POST['r'];
$url = 'https://api.paystack.co/transaction/verify/'.$ref;
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt(
	$ch, CURLOPT_HTTPHEADER, [
		'Authorization: Bearer sk_live_da1bc8fc9a7ea703025a7f927d3c2ec16c7215f6'
	]
);
$request = curl_exec($ch);
curl_close($ch);
$result = array();
if ($request) {
	$result = json_decode($request, true);
}
if (array_key_exists('data', $result) && array_key_exists('status', $result['data']) && ($result['data']['status'] === 'success')) {
	$_SESSION["tekky_ref"] = $ref;
	echo "0";
}
else{
	echo "Transaction unsuccessful.";
}
exit();
?>