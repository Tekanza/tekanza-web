<?php
if (isset($_GET["url"])){
    $file = $_GET["url"];
print '<!doctype html>
<head>
    <title>Viewing - '. $file .'</title>
	<meta http-equiv=Content-Type content="text/html; charset=ISO-8859-1">
	<meta name="robots" content="noindex, nofollow">
	<meta name="googlebot" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css" />
</head>
<body>
<div class="container" style="margin-top:25px;">
'; include __DIR__ ."/../docs/". $file .'.html'; print'
<p>Viewed on '. date("d M, Y") .'</p></div>
</body>';
}else{
    print "Please go back and try using another link to this page.";
}
?>