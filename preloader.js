
/* preloader */

;(function () {
	var preloaderDiv = document.createElement('div');
	preloaderDiv.innerHTML = `
		<style>
            #preloader_preload {
                display: block;
                z-index: 99999;
                height: 100vh;
                background: url('images/tk_loadr.gif') center center no-repeat;
                background-color: #000;
                position: fixed;
                top: 0;
                width: 100%;
                left: 0;
            }

            body.preloader {
              overflow: hidden;
              height: 100vh;
            }
        </style>
        <div id="preloader"><div id="preloader_preload"></div></div>
	`;
	
	document.body.insertBefore(preloaderDiv, document.body.firstChild);

    var preloaderStart = document.getElementById("preloader_preload");

    document.body.classList.add('preloader');

    function fadeOutFunction(el) {
        el.style.opacity = 1;

        var preloaderEngine = setInterval(function () {
            
            document.body.classList.remove('preloader');

            el.style.opacity = el.style.opacity - 0.03;

            if (el.style.opacity <= 0.03) {
                clearInterval(preloaderEngine);
                preloaderStart.style.display = "none";
            }

        }, 10);
    }

    window.onload = function () {
        setTimeout(function () {
            fadeOutFunction(preloaderStart);
        }, 80);
    };
})();

/* END preloader */