<style>
    .positioning {
        text-align: justify;    
    }
    
    @media (max-width: 991.98px) { 
        .positioning {
            text-align: center;
        }
    }
</style>

<div class="container-fluid positioning bg-dark">
<div class="row border-bottom">
<div class="col-md-3 my-3">
<p class="img_tek"><img src="images/tek_3_3.png" title="Tekanza" alt="Tekanza" style="width:50%"></p>
<p class="footer_abt text-white">We are a technological and innovative enterprise company, incorporated in Nigeria, with aim to transform business operations, develop solutions to the growing needs of the society through the use of technology and provide tools and platforms for digital awareness and engagements.</p>
</div>
<div class="col-md-3 my-3">
<div class="heady my-2"><b>USEFUL LINKS</b></div>
<p class="tek-bottom-contact-nav"><a href="about">The Company</a></p>
<p class="tek-bottom-contact-nav"><a href="about">About Us</a></p>
<p class="tek-bottom-contact-nav"><a href="#Careers/">Careers</a></p>
<p class="tek-bottom-contact-nav"><a href="#Foundation/">Foundation</a></p>
<p class="tek-bottom-contact-nav"><a href="contact">Contact Us</a></p>
<p class="tek-bottom-contact-nav"><a href="#FAQs/">FAQs</a></p>
<p class="tek-bottom-contact-nav"><a href="contact">Help Desk</a></p>
<p class="tek-bottom-contact-nav"><a href="contact">Support</a></p>
</div>
<div class="col-md-3 my-3">
<div class="heady my-2"><b>FEATURES</b></div>
<p class="tek-bottom-contact-nav"><a href="#more_about_tekanza_net">Computer Networking</a></p>
<p class="tek-bottom-contact-nav"><a href="more_about_tekanza_web">Web Development</a></p>
<p class="tek-bottom-contact-nav"><a href="#App_development/">App Development</a></p>
<p class="tek-bottom-contact-nav"><a href="#Branding/">Branding & Identity</a></p>
<p class="tek-bottom-contact-nav"><a href="#Digital_marketing/">Digital Marketing</a></p>
<p class="tek-bottom-contact-nav"><a href="#our-bonuses">Bonus Offer</a></p>
</div>
<div class="col-md-3 my-3">
<div class="heady my-2"><b>CONTACT INFO</b></div>
<p class="tek-bottom-contact-nav"><a href="tel:09060976211"><i class="fa fa-phone-square" aria-hidden="true"> +2349060976211</i></a></p>
<p class="tek-bottom-contact-nav"><a href="mailto:info@tekanza.com"><i class="fa fa-envelope-square"> info@tekanza.com </i></a></p> <p class="tek-bottom-contact-nav"><a href="mailto:info.tekanza@gmail.com"><i class="fa fa-envelope-square"> info.tekanza@gmail.com</i></a></p><p class="tek-bottom-contact-nav"><a href="mailto:info.tekanza@gmail.com"><i class="fa fa-envelope-square"> support@tekanza.com</i></a></p>
</div>
<div class="col-md-3 my-3">
<div class="heady my-2"><b>FOLLOW US:</b></div>
<p class="tek-contact-icon"><a href="https://www.facebook.com/tekanz"><i class="fa fa-facebook-square"></i></a></p>
<p class="tek-contact-icon"><a href="https://twitter.com/TekanzaLLC"><i class="fa fa-twitter-square"></i></a></p>
<p class="tek-contact-icon"><a href="https://instagram.com/tekanzaofficial"><i class="fa fa-instagram"></i></a></p>
<p class="tek-contact-icon"><a href="https://linkedin.com/tekanzaLLC"><i class="fa fa-linkedin-square"></i></a></p>
<p class="tek-contact-icon"><a href="#"><i class="fa fa-pinterest-square"></i></a></p>
<div class="">
<h4 class="footer-heading mb-4"><i class="fa fa-rss"></i> Subscribe Newsletter</h4>
<form action="#" method="post">
<div class="input-group mb-3">
<input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" required aria-label="Enter Email" aria-describedby="button-addon2">
<div class="input-group-append">
<button class="btn-submit tek-background-blue text-white" type="submit" id="submit">Send</button>
</div>
</div>
</div>
</div>
</div>
<div class="p-3" style="margin-left: auto; margin-right: auto;">
<center class="mx-auto"><p class="align_me text-center text-white">&copy; &nbsp; <script>document.write(new Date().getFullYear());</script> &nbsp;Tekanza. &nbsp; All Right Reserved. &nbsp; | &nbsp; <a href="#Copyright_notification/"> Copyright Notification</a> &nbsp; | &nbsp; <a href="#Terms_of_use/">Terms of Use</a> &nbsp; | &nbsp; <a href="#Privacy_policy/">Privacy Policy</a></p></center>
</div>
</div>
  
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/js_2/jquery-ui.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/js_2/jquery.swipebox.min.js"></script>
  <script src="js/js_2/move-top.js"></script>
  <script src="js/numscroller-1.0.js"></script>
  <script src="js/js_2/simplePlayer.js"></script>
  <script src="js/main.js"></script>
  </body>
  </html>