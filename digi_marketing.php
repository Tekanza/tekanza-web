<?php include"query.php";?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Digital Marketing</title>
<meta charset="utf-8">
<meta name="description" content="Tekanza ICT Hub">
<meta name="keywords" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="css/about">
<link rel="stylesheet" href="css/about_responsive"> 
<?php head();?>
</head>
 <!--Body-->
 <body>
  <script src="preloader.js"></script>
 
          <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>
          </div>
        </div>
      </div>   
    </header>
   <div class="site-blocks-cover overlay" style="background-image: url(images/tek-bg-img_1.png);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-12" data-aos="fade-up" data-aos-delay="400">
                        
            <div class="row justify-content-center mb-4">
              <div class="col-md-8 text-center">
                <h1>Marketing</h1>
                <p class="lead mb-5">We design marketing platform with quality services delivery.</p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>  

    <section class="site-section border-bottom">
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary icon-shopping_cart"></span></div>
              <div>
                <h3><u>Marketing</u></h3>
                <p><h6>1. Digital Marketing</h6></p>
				<ul>
				<li>Search Engine Optimization (SEO)</li>
				<li>Search Engine Marketing(SEM)</li>
				<li>Per Per click Advertising (PPC)</li>
				<li>Social Media Marketing</li>
				<li>Mobile Media Marketing</li>
				<li>Email Marketing/Newsletters</li>
				<li>Copywriting Services</li>
				</ul>
				<p><h6>2. Analytic Services</h6></p>
				<ul>
				<li>Traffic for SERP, Social, Referals and paid contents.</li>
				<li>Action Conversions Conversion Rate Optimization (ACCRO)</li>
				<li>Click-through Rate (CTR)</li>
				<li>Engagement (Social Media)</li>
				<li>Site Load Speed on CDNs, Images and Site Codes</li>
				</ul></p>
				<p><h6>3. Branding</h6></p>
				<ul>
				<li>Graphic Design</li>
				<li>Corporate Identity</li>
				</ul>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary icon-laptop2"></span></div>
              <div>
                <h3>System Networking</h3>
                <p><h6>1. Network Infrastructure</h6></p>
				<ul>
				<li>Networking Services for home, SME, and large scale enterprises</li>
				<li>Server Infrastructure</li>
				<li>IP Surveilance</li>
				<li>Video and Tele-conferencing</li>
				</ul>
				<p><h6>2. Security Solution</h6></p>
				<ul>
				<li>Database Security</li>
				<li>Cloud Sharing Networks</li>
				<li>Application & File Security</li>
				<li>Online Banking</li>
				</ul>
				<p><h6>3. Web Design/Database Mgt</h6></p>
				<p><h6>4. Software Development</h6></p>
				<ul>
				<li>Mobile Applications</li>
				<li>Desktop Applications</li>
				<li>Web-based Applications</li>
				</ul>
              </div>
            </div>
          </div>
    </section>
    

    <section class="site-section border-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4 mb-4" data-aos="fade-up">
            <div class="p-3 box-with-humber">
              <div class="number-behind">01.</div>
              <h2>Web Design</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et praesentium eos nulla qui commodi consectetur beatae fugiat. Veniam iste rerum perferendis.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="100">
            <div class="p-3 box-with-humber">
              <div class="number-behind">02.</div>
              <h2>Web Apps</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et praesentium eos nulla qui commodi consectetur beatae fugiat. Veniam iste rerum perferendis.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="200">
            <div class="p-3 box-with-humber">
              <div class="number-behind">03.</div>
              <h2>WordPress</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et praesentium eos nulla qui commodi consectetur beatae fugiat. Veniam iste rerum perferendis.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="300">
            <div class="p-3 box-with-humber">
              <div class="number-behind">04.</div>
              <h2>Web Design</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et praesentium eos nulla qui commodi consectetur beatae fugiat. Veniam iste rerum perferendis.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="400">
            <div class="p-3 box-with-humber">
              <div class="number-behind">05.</div>
              <h2>Web Apps</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et praesentium eos nulla qui commodi consectetur beatae fugiat. Veniam iste rerum perferendis.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="500">
            <div class="p-3 box-with-humber">
              <div class="number-behind">06.</div>
              <h2>WordPress</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et praesentium eos nulla qui commodi consectetur beatae fugiat. Veniam iste rerum perferendis.</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="site-section testimonial-wrap" data-aos="fade">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-8 text-center">
            <h2 class="text-black h1 site-section-heading text-center">Testimonials</h2>
          </div>
        </div>
      </div>
      <div class="slide-one-item home-slider owl-carousel">
          <div>
            <div class="testimonial">
              
              <blockquote class="mb-5">
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>

              <figure class="mb-4 d-flex align-items-center justify-content-center">
                <div><img src="images/person_3.jpg" alt="Image" class="w-50 img-fluid mb-3"></div>
                <p>John Smith</p>
              </figure>
            </div>
          </div>
          <div>
            <div class="testimonial">

              <blockquote class="mb-5">
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>
              <figure class="mb-4 d-flex align-items-center justify-content-center">
                <div><img src="images/person_2.jpg" alt="Image" class="w-50 img-fluid mb-3"></div>
                <p>Christine Aguilar</p>
              </figure>
              
            </div>
          </div>

          <div>
            <div class="testimonial">

              <blockquote class="mb-5">
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>
              <figure class="mb-4 d-flex align-items-center justify-content-center">
                <div><img src="images/person_4.jpg" alt="Image" class="w-50 img-fluid mb-3"></div>
                <p>Robert Spears</p>
              </figure>
            
            </div>
          </div>

          <div>
            <div class="testimonial">

              <blockquote class="mb-5">
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>
              <figure class="mb-4 d-flex align-items-center justify-content-center">
                <div><img src="images/person_5.jpg" alt="Image" class="w-50 img-fluid mb-3"></div>
                <p>Bruce Rogers</p>
              </figure>

            </div>
          </div>

        </div>
    </section>
<!----------footer---------->
<?php footer();?>