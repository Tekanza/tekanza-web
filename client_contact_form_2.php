<div class="client-form formated-form" style="margin-top:-20px;"><div class="section-title" style="color: #009fd2"><h3>Client Request Form</h3>
<p>Tell us what we need to know about your project, and how we can work with you.</p> </div> <div class="row">

<div class="col-md-6" id="md2">
<label class="sel_opt_int">Area of Interest: *</label>
<div class="select_row col-md-12">
<select  class="select_row" id="select_row" name="j001" required autofocus type="select">
<option class="sel_row_1" value="0">Select Area of Interest</option>
<option class="sel_row_1" value="1">Content Management (CMs)</option>
<option class="sel_row_1" value="2">Blog</option>
<option class="sel_row_1" value="3">Personal Website (non-trading)</option>
<option class="sel_row_1" value="4">Personal Website (trading)</option>
<option class="sel_row_1" value="5">E-commerce</option>
<option class="sel_row_1" value="6">Web Application</option>
</select>
</div>
<label class="sel_opt_int">Business Type :* </label>

<div class="select_row col-md-12">
<select class="select_row" name="j002" 	required id="name">
<option id="biz_ty" ="0">Please Select</option>
<option class="biz_ty" value="1">Charity or Non-profit</option>
<option class="biz_ty" value="2">Limited Liability Company</option>
<option class="biz_ty" value="3">International Incorporation</option>
<option class="biz_ty" value="4">Sole Proprietor</option>
<option class="biz_ty" value="5">Other</option>
</select>
</div>

<label class="sel_opt_int">Your Budget: *</label>
<div class="select_row col-md-12">
<input type="text" class="select_row col-md-12" placeholder="Numbers only" name="j003" required>
</div>

</div>

<div class="col-md-6" id="md2">
<label class="select_row" >Project Start Date: *</label>

<div class="select_row col-md-12">
<select class="select_row" id="biz_ty" name="j004" required>
<option id="biz_ty" value="0">Please Select</option>
<option id="biz_ty" value="1">Immediately</option>
<option id="biz_ty" value="2">One week time</option>
<option id="biz_ty" value="3">2- 3 week time</option>
<option id="biz_ty" value="4">0ne month time</option>
</select>
</div>
<label class="select_row" class="sel_opt_int">Project Description: *</label>
<div class="col-md-12">
<textarea class="select_row col-md-12" name="j005" required placeholder="Description:">
</textarea>
</div>

</div>

<div class="col-md-12">
<label class="sel_opt_int">How Do We Contact You?</label>
<div class="row">
<div class=" col-md-6">
<label class="select_row" class="sel_opt_int">Via Phone</label>
<input type="text" name="j006" class="select_row" placeholder="Numbers only" required>
</div>
<div class=" col-md-6">
<label class="select_row" class="sel_opt_int">Via Email</label>
<input type="email" name="j007" class="select_row" required placeholder="E-mail Address:">
</div>
</div>
<div class="text-center">
<input name="j008" type="hidden" value="#coded" />

</div>
</div>


</div>
</div>