<?php include"query.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Contact Us</title>
<meta charset="utf-8">
<meta name="description" content="Tekanza ICT Hub">
<meta name="keywords" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="css/about.css">
<link rel="stylesheet" href="css/about_responsive.css">
<?php head();?>
<!--<script src="preloader.js"></script>-->

<div class="site-blocks-cover overlay" style="background-image: url(images/contact_img_3.png); background-size: cover;" data-aos="fade">
  <div class="container">
	<div class="row align-items-center justify-content-center text-center">

	  <div class="col-md-12" data-aos="fade-up" data-aos-delay="400">
					
		<div class="row justify-content-center mb-4">
		  <div class="col-md-8 text-center">
			<h1 class="up_c">Contact Us</h1>
			<p class="lead mb-5"></p>
		  </div>
		</div>

	  </div>
	</div>
  </div>
</div>  
    <!-- Courses section -->
	<section class="contact-page spad pt-0">
		<div class="container">
			
			<div class="map-section">
			<div class="row">
				<div class="contact-info-warp p-3 col-md-6">
					<div class="contact-info">
						<h1><a href="#address"><i class="fa fa-map-marker"></i></a></h1>
						<p>Tonimas Estate, 181 Etta Agbor Road, Calabar, Cross River State, Nigeria, West Africa. 540242. </p>
					</div>
					<div class="contact-info">
						<h1><a href="#phone"><i class="fa fa-phone-square" aria-hidden="true"></i></a></h1>
						<p>+2349060976211</p>
					</div>
					<div class="contact-info">
						<h1><a href="#email"><i class="fa fa-envelope"></i></a></h1>
						<p> info@tekanza.com, info.tekanza@gmail.com</p>
					</div>
				</div>
				
				<!-- Google map -->
	<div class="gmap col-md-6 p-3">
		<iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d3974.966366669068!2d8.336540213984613!3d4.945243440867426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e2!4m5!1s0x106786427c3ece5d%3A0x958913f176402b5a!2s59%20Etta%20Agbo%20Road%2C%20Calabar!3m2!1d4.9452381!2d8.3387289!4m3!3m2!1d4.9452701999999995!2d8.3389435!5e0!3m2!1sen!2sng!4v1594054250628!5m2!1sen!2sng" width="100%" height="583" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			</div>
		</div>
		</div>
<div class="client-modal" id="client-modal">
<button class="client-modal-close">&times;</button>
	<div class="modalContainer">
		<form class="form1" id="f_fWUi"></form>
	</div>
</div>
<div class="contact-form">
<div id="m" class="reply"></div>
<div class="contact-frame"><?=
	defaultContactForm();
?></div></div>

<script type="text/javascript">
	function Submit(){
	var select=document.getElementById("c_fWUi");
if(select.value===""){
	//if the "Please select Option" is selected display error.
	alert("Please select an option from the select box!"); 
	return false;
}
return true;
}
</script>
</div>

<script type="text/javascript">
const button='<div id="md"></div><div class="text-center"><button id="btnbtn" style="border-radius:5px;padding:20px 40px;" class="btn btn-primary">Continue</button></div> ';
const modal = document.getElementById("client-modal");
let printError = function(error,explicit) {
    console.log(`[${explicit ? 'EXPLICIT' : 'INEXPLICIT'}] ${error.name}: ${error.message}`);
}
var chn = function(){
    $.ajax({url:"query?kl=r00",method:"get",dataType:"json",success:function(data){document.getElementById("f_fWUi").innerHTML = data.t+button; }, });
}
$('#c_fWUi0').change(function(){
let opval = document.getElementById('c_fWUi0').value;
if(opval=="r_fWUi0"){ modal.style.display = "block";document.getElementById("f_fWUi").innerHTML = "<pre>Loading form...</pre>"; chn(); }});
$(".client-modal-close").click(function(){ $("#client-modal").hide(20); $("#f_fWUi").empty(); });
$(document).keyup(function(i){if(i.keyCode==27){$("#client-modal").hide(0); $("#f_fWUi").empty();} });
$("#f_fWUi").submit(function(p){
	p.preventDefault();
	var d=$("#f_fWUi").serialize();
	$("#f_fWUi").css({"opacity":".25"});
	$.post("query",d,function(data,status,xhr){
	    data = JSON.parse(data);
        try{
            document.getElementById("f_fWUi").innerHTML = data.t+button;
        }catch(e){
            if(e instanceof TypeError){
                printError(e,true);
            }else{
                printError(e,false);
            }
        }
	    $("#f_fWUi").css({"opacity":"1"});
        if(data.count==3){document.getElementById("btnbtn").style.display="none";}
	});
});
$(".comment-form").submit(function(e){e.preventDefault();var d = $(".comment-form").serialize();$.ajax({data: d,url: "query",method: "post",dataType: "json",success: function(data){ $(".reply").fadeIn(2000).addClass("alert toaster"); if(data.type == 0){ $(".reply").addClass("alert-success"); } else{ $(".reply").addClass("alert-danger"); } $(".contact-frame").slideUp(1500); $(".reply").html(data.t);  $(".comment-form")[0].reset(); }, });});
</script>
</section>
<?=
	footer();
?>
