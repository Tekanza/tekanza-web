<!DOCTYPE html>
<html>
<head>
<title>Network Infrastructure</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<style>

body {font-family: Arial, Helvetica, sans-serif;}

.modal {
  display: none; 
  position: fixed; 
  z-index: 1; 
  padding-top: 100px; 
  left: 0;
  top: 0;
  width: 100%; 
  height: 100%; 
  overflow: auto;
}
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 80%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}


@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

.close {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.modal-header {
  padding: 2px 16px;
  background-color: #000000;
  color: white;
}

.modal-body {padding: 2px 16px; line-height:1.5;}

.modal-footer {
  padding: 2px 16px;
  background-color: #000000;
  color: white;
  margin-bottom:6em;
}
.modal-footer a{
	color:#ffffff;
}
.modal-footer a:link{
	text-decoration:none;
}
.modal-footer a:hover{
	color:#0000ff;
}
.hjvhv{
	float:left;
	width:38.9%;
	padding:5px;
	margin-left:5em;
	margin-bottom:1em;
	background-color:gray;
	
}
.img_@::after{
	content:"";
	clear:both;
	display:table;
}
.para{
	background-color:#000000;
	color:#ffffff;
	border-radius:10px;
	border-color: 10px solid blue;
	padding-left:25px;
	padding-right:25px;
	text-align:justify;
	font-family:Time New Roman;
}
.para ul > li a{
	color:#0000ff;
	font-size:1.5em;
	line-height:1.5em;
}
.para ul > li a:link{
	text-decoration:none;
}
.para ul > li a:hover{
	color:#900;
}
</style>
</head>

<button id="myBtn"><i class="fa fa-spinner"> Read More</i></button>

<div id="myModal" class="modal">	
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <center><h2>NETWORK INFRASTRUCTURE</h2></center>
    </div>
    <div class="modal-body">
	<div class="img_@">
	<div class="hjvhv">
	<img src="images/img_@_2.jpg" alt="ICT HUB" style="width:100%; height:200px"></div>
	<div class="hjvhv">
	<img src="images/img_@_2.jpg" alt="ICT HUB" style="width:100%; height:200px"></div>
	<div class="para">
<p>Tekanza ICT Hub is a sophisticated network infrastructure company designed to promote and increase the productivity of any enterprise through quality network services delivery.

<p>As a business enterprise, government, and any institutions, understanding the importance of network infrastructure design and developing an implementation plan based on the needs and priorities of your firm is very crucial to long-term success.</p> <p>Therefore, hiring and partnering with Tekanza ICT Hub will provides a reliable network infrastructure plan to you while giving the flexibility to outsource and augment your company services optimally. Our team is well equipped with the infrastructural design and development and they can manage your network infrastructure design and implementation from the ground up, linking and connecting multiples network infrastructural devices, bringing locations and powering the business systems your company needs to build.</p>

Our network infrastructure consultants and engineers have years of experience in architecting, managing network cloud system, servers and client infrastructure, on-premises, etc that are designed to the specific business goals and models. Every infrastructure designed is uniquely given to each client aims at individual goals and objectives for proper optimized operations. Tekanza ICT Hub has an expertise in development and implementing solutions.

<h3>The area of specialties includes;</h3>
<ul>
<li><button><a href="#">Implementing and managing of cloud computing and storage.</a></li></button>
<li><button><a href="#">Server and Client installation</li></button>
<li><button><a href="#">On-premise server computing and storage</a></li></button>
<li><button><a href="#">Cabling infrastructure</a></li></button>
<li><button><a href="#">Physical and logical connectivity</a></li></button>
<li><button><a href="#">Wireless installation and management</a></li></button>
<li><button><a href="#">Business continuity, disaster recovery and data back-up</a></li></button>
<li><button><a href="#">Office relocation or new location built-out</a></li></button>
<li><button><a href="#">Geo location</a></li></button>
<li><button><a href="#">VOIP and on-premises telephone system</a></li></button>
<li><button><a href="#">Centralized prints services</a></li></button>
<li><button><a href="#">Application integration</a></li></button>
<li><button><a href="#">Acquisition integration</a></li></button>
<li><button><a href="#">Email services</a></li></button>
<li><button><a href="#">Firewall, antivirus, antispam and other data security and protection hardware and software.</a></li></button>
</ul>
</p>
</div>

    </div>
    <div class="modal-footer">
      <h3><a href="index"><i class="fa fa-home"> Home</i></a>  | <a href="popup"><i class="fa fa-spinner"> More </i></a></h3>
    </div>
  </div>

</div>

<script>

var modal = document.getElementById("myModal");


var btn = document.getElementById("myBtn");

var span = document.getElementsByClassName("close")[0];

btn.onclick = function() {
  modal.style.display = "block";
}


span.onclick = function() {
  modal.style.display = "none";
}


window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>


