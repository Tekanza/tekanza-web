<?php include"query.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Portfolio</title>
<meta charset="utf-8">
<meta name="description" content="Tekanza ICT Hub">
<meta name="keywords" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="css/about">
<link rel="stylesheet" href="css/about_responsive">
<?php head();?>
<script src="preloader.js"></script>
    <div class="site-blocks-cover overlay" style="background-image: url(images/contact_img_3.png); background-size: cover;" data-aos="fade">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">
          <div class="col-md-12" data-aos="fade-up" data-aos-delay="400">
            <div class="row justify-content-center mb-4">
              <div class="col-md-8 text-center">
                <h1 class="up_c">Portfolio</h1>
                <p class="lead mb-5"></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
<div class="main_con">
<div class="row_1">
  <div class="column_1">
    <div class="content_1">
      <img src="images/isdms_.png" alt="Partner" style="width:80%">
      <p><h3 align="center" > Integrated School Digital Management System (ISDMS) </h3></p>
    </div>
	  </div>
    <div class="column_1">
    <div class="content_1">
      <img src="images/img_h_1.png" alt="Partner" style="width:80%" >
      <p><h3 align="center" > UGenome </h3></p>
    </div>
  </div>
</div>
</div>
<div class="row justify-content-center mb-5">
<div class="col-md-7 text-center border-primary">
         <h2 style="color:#000" class="" data-aos="fade">Project Statistics</h2>
          </div>
        </div>
   <div class="statis">
<div class="stats" id="stats">
	<div class="container">
		<div class="stats-info">
			<div class="col-md-3 col-xs-3 stats-grid slideanim">
				<i class="fa fa-key" aria-hidden="true"></i>
				<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='50' data-delay='.5' data-increment="1">50</div>
				<h4 class="stats-info">NEW PROJECT</h4>
			</div>
			<div class="col-md-3 col-xs-3 stats-grid slideanim">
				<i class="fa fa-lock" aria-hidden="true"></i>
				<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='0' data-delay='.5' data-increment="1"></div>
				<h4 class="stats-info">PENDING PROJECT</h4>
			</div>
			<div class="col-md-3 col-xs-3 stats-grid slideanim">
				<i class="fa fa-briefcase" aria-hidden="true"></i>
				<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='0' data-delay='.5' data-increment="1">0</div>
				<h4 class="stats-info">ABANDONED PROJECT</h4>
			</div>
			<div class="col-md-3 col-xs-3 stats-grid slideanim">
					<i class="fa fa-check-square" aria-hidden="true"></i>
				<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='20' data-delay='.5' data-increment="1">20</div>
				<h4 class="stats-info">COMPLETED PROJECT</h4>
			</div>
		</div>
	</div>
</div>
 </div>
<div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center border-primary">
         <h2 style="color:#000" class="" data-aos="fade">Recent Projects</h2>
          </div>
        </div>
      </div>
<div class="main_new">
<div class="row_new">
  <div class="column_new">
    <div class="content_new">
      <img src="images/img_h_1.png" alt="" style="width:100%">
    </div>
  </div>
    <div class="column_new">
    <div class="content_new">
      <img src="images/img_h_1.png" alt="" style="width:100%">
      </div>
  </div>
    <div class="column_new">
    <div class="content_new">
      <img src="images/img_h_1.png" alt="" style="width:100%">
    </div>
  </div>
    <div class="column_new">
    <div class="content_new">
      <img src="images/img_h_1.png" alt="" style="width:100%">
    </div>
  </div>
</div>
</div>
<?php footer();?>