<!DOCTYPE html>
<html lang="en">
<head>
<title>Foundation</title>
<meta charset="utf-8">
<meta name="description" content="Tekanza ICT Hub">
<meta name="keywords" content="Web Development, App Development, Network Infrastructure, Branding & Identity">
<meta name="author" content="Dr. John Robert, Engr. Aniefiok Udoh, Engr. Emmanuel Ekunke">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="css/about">
<link rel="stylesheet" href="css/about_responsive">

<!--Body--> 
<script src="preloader.js"></script>  
<h2><center class="careers" style="color:#000">page not found!</center></h2>
<h6><a href="index">Back to home page</a></h6>
</html>