<?php include"query.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>About Us</title>
<meta charset="utf-8">
<meta name="description" content="Tekanza ICT Hub">
<meta name="keywords" content="Web Development, App Development, Network Infrastructure, Branding & Identity">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="css/about.css">
<link rel="stylesheet" href="css/about_responsive.css">
<?php head();?>
<style>
	.just_1,.just_2{
	text-align:justify;
	}
	</style>
  <script src="preloader.js"></script>
  
    <div class="site-blocks-cover overlay" style="background-image: url(images/tek_we.png);" data-aos="fade">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">
          <div class="col-md-12" data-aos="fade-up" data-aos-delay="400">          
            <div class="row justify-content-center mb-4">
              <div class="col-md-8 text-center">
                <h1 class="up_c">About Us</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> 

 <div class="site-section">
     <style>
        @media (max-width: 767.98px) {
            .img {
                height: 200px;
            }
            .contain {
                height: 450px;
                text-align: justify;
            }
            .card {
                border: none !important;
                background-color: aliceblue;
            }
            .card:hover {
                background-color: #009fd1;
                color: white;
            }
        }
        @media (min-width: 768px) {
            .img {
                height: 250px;
            }
            .contain {
                height: 500px;
                text-align: justify;
            }
            .card {
                border: none !important;
                background-color: aliceblue;
            }
            .card:hover {
                background-color: #009fd1;
                color: white;
            }
        }
    </style>
 <!-- <center><button class="break center"><strong>EXECUTIVE SUMMARY</strong></button></center>
<div class="content">
  <p>Tekanza is an IT based institution. A Limited Liability company and a trademark of Tekanza ICT Hub, dedicated towards providing quality and excellent services and IT Solutions in areas of technology. We aim to provide top-notch products and services that fills the gap in the technology market. Our products will not only engage our clients positively and productively but will also provide the ease of doing business and managing services concurrently. As a team at Tekanza we are focused in incorporating our individual strengths, professionalisms, knowledge and our ingenuities towards providing effective and quality services within globally recognized standards.</p> <p> We look forward to maintaining a suitable and comprehensive service delivery that exceeds our clients expectations, also according the mutual and beneficial business relationship with our clients, shareholders and users. </p>
</div>-->

     <div class="container mt-5">
        <div class="row">
            <div class="col-md-6 card p-3">
                <h5 class="py-2" style="font-size: 20px;">Who We Are?</h5>
                <p style="text-align: justify;">We are a technological and innovative enterprise and company, incorporated in Nigeria, with aim to
                    transform business operations, develop solutions to the growing needs of the society through the use
                    of technology and provide tools and platforms for digital awareness and engagements.</p>
            </div>
            <div class="col-md-6 card p-3">
                <h5 class="py-2" style="font-size: 20px;">What We Do</h5>
                <p style="text-align: justify;">As a technology company we plan and carry out boots-on-the-ground projects to accomplish our
                    objectives thereby engaging the synergy of innovative and creative minds in areas of Information
                    Technology, Application/Software Development, Computer Networking Technology, Web Development
                    Technology, Database Management Technology and Artificial Intelligence.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center py-5" style="font-size: 30px;">
                    OUR CORE VALUES
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">                
                    <img class="img" src="images/diversity.jpeg" width="100%" alt="diversity">
                    <div class="card-body">
                        <h5 class="card-title">Diversity</h5>
                        <p class="card-text">We respect and appreciate differences in ethnicity, gender, age, physical abilities, sexual orientation, education and religion. We all contribute with diverse perspectives, experience, knowledge and culture.</p>
                    </div>         
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">
                    <img class="img" src="images/innovation.jpeg" width="100%" alt="diversity">
                    
                    <div class="card-body">
                        <h5 class="card-title">Innovation</h5>
                        <p class="card-text">Innovation is part of our DNA. We value original thinking and the passion to attack difficult challenges, seeking creative ways to solve tough problems.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">        
            
                    <img class="img" src="images/teamwork.png" width="100%"  alt="diversity">
            
                <div class="card-body">
                    <h5 class="card-title">Team Work</h5>
                    <p class="card-text">Collaboration is basic human nature, Teamwork makes the dream work. We work together to bring our passions and expertise for the common good in commitment towards our goals and objectives.</p>
                </div>
            
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">
                    <img class="img" src="images/accountability.jpg" width="100%"  alt="diversity">
                
                    <div class="card-body">
                        <h5 class="card-title">Accountability</h5>
                        <p class="card-text">We believe in reliability and taking responsibilities for our actions while maintaining highest standards of integrity and fiscal responsibility. We value the ability of our staff and organization to honor our commitments, to clients and to each other.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">
                    <img class="img" src="images/quality.png" width="100%"  alt="diversity">
                
                    <div class="card-body">
                        <h5 class="card-title">Quality</h5>
                        <p class="card-text">As a priority, we strive to provide high quality and outstanding services and products that meet the expectations and requirements of our customers and clients.</p>
                    </div>                       
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">
                    <img class="img" src="images/integrity.png" width="100%"  alt="diversity">                 
                
                    <div class="card-body">
                        <h5 class="card-title">Integrity</h5>
                        <p class="card-text">We are commited to honesty and transparency, open and not taking advantage of others. We uphold strict adherence to our moral or ethical code of conduct.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 contain">
                    <img class="img" src="images/impact.jpeg" width="100%"  alt="diversity">                 
                
                    <div class="card-body">
                        <h5 class="card-title">Impact</h5>
                        <p class="card-text">We strive to make positive influence on our society. We build and maintain social capital through our operations; the goods and services we provide.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
  </div>
    </div>
    </div>
    </div>
    
    <section class="site-section testimonial-wrap" data-aos="fade">
      </section>

<!--- Executive_collapse-->
<script>
var coll = document.getElementsByClassName("break");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    } 
  });
}
</script>
<?php footer();?>
