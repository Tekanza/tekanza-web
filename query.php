<?php
session_start();

/**
*
**/

class dbFunctions{
	private $conn;
	public $username="tekanzac";
	public $dbname="tekanzac_db";
	public $password="#tekanza19";
	public $host="localhost";
	public function getDbconnection(){
		$this->conn=null;
		try{
			$this->conn=new PDO("mysql:host=".$this->host.";dbname=".$this->dbname,$this->username,$this->password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
			$this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES,true);
		}
		catch(PDOException $e){
			$e->getMessage();
		}
		return $this->conn;
	}
	public function sqlDbconnection(){
		return mysqli_connect($this->host,$this->username,$this->password,$this->dbname);
	}
}

$dbase = new dbFunctions;
$db = $dbase->getDbconnection();
$qi = $dbase->sqlDbconnection();

function head(){
print '<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon" style="min-height:2px">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" href="css/css_2/jquery-ui.css">
<link rel="stylesheet" href="css/css_2/swipebox.css">	
<link rel="stylesheet" href="fonts/icomoon/style.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="css/bootstrap-datepicker.css">
<link rel="stylesheet" href="fonts/flaticon/font/flaticon">
<link rel="stylesheet" href="css/aos.css">
<link rel="stylesheet" href="css/style.css?v='.time().'">
<link rel="stylesheet" type="text/css" href="css/css_2/main_styles.css?v='.time().'">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
</head><body>
<div class="s_container">';
	include_once"header.php";
}

function footer(){
	include_once"footer.php";
}

function query(){
	
}

function optionsbvac(){
	$_more="";
	foreach(country_value() as $bvac){
		$_more .="<option value=\"". $bvac["value"] ."\">". $bvac["name"] ."</option>";
	}
	return $_more;
}

function defaultContactForm(){
print'<div class="section-title">
					<center style="color:#009fd1"><h3>Get In Touch With Us</h3>
				</div>
<form class="comment-form --contact" method="" action="POST" enctype="multi-part/form-data">
					<div class="row">
						<div class="col-lg-4">
<input type="text" required name="name" placeholder="Name/Company/Organization:*">
						</div>
						<div class="col-lg-4">
<input type="email"  name="email" required placeholder="Email Address:*">
						</div>
						<div class="col-lg-4">
<input type="text" required name="phone" placeholder="Phone No.:*">
						</div>	
						<div class="col-lg-4">
<select id="select" required name="country" type="select">
	<option value="">Select Country *</option>'.
	optionsbvac().'
</select>
</div>
<div class="col-lg-8">
<select id="c_fWUi0" class="ackky" name="reason" required>
	<option>Select Contact Reason*</option>
	<option class="ackky" value="complaints">Complaints</option>
	<option class="ackky" value="support">Support</option>
	<option class="ackky" value="help desk">Help Desk</option>
	<option class="ackky" id="r_fWUi0" value="r_fWUi0">Fill Project Request Form</option>
</select>
</div>

<div class="col-lg-12">
<textarea name="description" placeholder="Description:*" required></textarea>
<div class="text-center">
<input type="submit" id="site-btn" name="ictn" value="Submit" onclick="return Submit()" />
<!--<input class="reset" type="reset" id="site-btn" value="Reset" />-->
</div>
</div>
</div>
</form>';
}

function country_value(){ 
	return [ array( "value" => "AF", "name" => "Afghanistan" ), 		array( "value" => "AX", "name" => "Åland Islands" ), 		array( "value" => "AL", "name" => "Albania" ), 		array( "value" => "DZ", "name" => "Algeria" ), 		array( "value" => "AS", "name" => "American Samoa" ), 		array( "value" => "AD", "name" => "Andorra" ), 		array( "value" => "AO", "name" => "Angola" ), 		array( "value" => "AI", "name" => "Anguilla" ), 		array( "value" => "AQ", "name" => "Antarctica" ), 		array( "value" => "AG", "name" => "Antigua and Barbuda" ), 		array( "value" => "AR", "name" => "Argentina" ), 		array( "value" => "AM", "name" => "Armenia" ), 		array( "value" => "AW", "name" => "Aruba" ), 		array( "value" => "AU", "name" => "Australia" ), 		array( "value" => "AT", "name" => "Austria" ), 		array( "value" => "AZ", "name" => "Azerbaijan" ), 		array( "value" => "BS", "name" => "Bahamas" ), 		array( "value" => "BH", "name" => "Bahrain" ), 		array( "value" => "BD", "name" => "Bangladesh" ), 		array( "value" => "BB", "name" => "Barbados" ), 		array( "value" => "BY", "name" => "Belarus" ), 		array( "value" => "BE", "name" => "Belgium" ), 		array( "value" => "BZ", "name" => "Belize" ), 		array( "value" => "BJ", "name" => "Benin" ), 		array( "value" => "BM", "name" => "Bermuda" ), 		array( "value" => "BT", "name" => "Bhutan" ), 		array( "value" => "BO", "name" => "Bolivia, Plurinational State of" ), 		array( "value" => "BQ", "name" => "Bonaire, Sint Eustatius and Saba" ), 		array( "value" => "BA", "name" => "Bosnia and Herzegovina" ), 		array( "value" => "BW", "name" => "Botswana" ), 		array( "value" => "BV", "name" => "Bouvet Island" ), 		array( "value" => "BR", "name" => "Brazil" ), 		array( "value" => "IO", "name" => "British Indian Ocean Territory" ), 		array( "value" => "BN", "name" => "Brunei Darussalam" ), 		array( "value" => "BG", "name" => "Bulgaria" ), 		array( "value" => "BF", "name" => "Burkina Faso" ), 		array( "value" => "BI", "name" => "Burundi" ), 		array( "value" => "KH", "name" => "Cambodia" ), 		array( "value" => "CM", "name" => "Cameroon" ), 		array( "value" => "CA", "name" => "Canada" ), 		array( "value" => "CV", "name" => "Cape Verde" ), 		array( "value" => "KY", "name" => "Cayman Islands" ), 		array( "value" => "CF", "name" => "Central African Republic" ), 		array( "value" => "TD", "name" => "Chad" ), 		array( "value" => "CL", "name" => "Chile" ), 		array( "value" => "CN", "name" => "China" ), 		array( "value" => "CX", "name" => "Christmas Island" ), 		array( "value" => "CC", "name" => "Cocos (Keeling) Islands" ), 		array( "value" => "CO", "name" => "Colombia" ), 		array( "value" => "KM", "name" => "Comoros" ), 		array( "value" => "CG", "name" => "Congo" ), 		array( "value" => "CD", "name" => "Congo, the Democratic Republic of the" ), 		array( "value" => "CK", "name" => "Cook Islands" ), 		array( "value" => "CR", "name" => "Costa Rica" ), 		array( "value" => "CI", "name" => "Côte d'Ivoire" ), 		array( "value" => "HR", "name" => "Croatia" ), 		array( "value" => "CU", "name" => "Cuba" ), 		array( "value" => "CW", "name" => "Curaçao" ), 		array( "value" => "CY", "name" => "Cyprus" ), 		array( "value" => "CZ", "name" => "Czech Republic" ), 		array( "value" => "DK", "name" => "Denmark" ), 		array( "value" => "DJ", "name" => "Djibouti" ), 		array( "value" => "DM", "name" => "Dominica" ), 		array( "value" => "DO", "name" => "Dominican Republic" ), 		array( "value" => "EC", "name" => "Ecuador" ), 		array( "value" => "EG", "name" => "Egypt" ), 		array( "value" => "SV", "name" => "El Salvador" ), 		array( "value" => "GQ", "name" => "Equatorial Guinea" ), 		array( "value" => "ER", "name" => "Eritrea" ), 		array( "value" => "EE", "name" => "Estonia" ), 		array( "value" => "ET", "name" => "Ethiopia" ), 		array( "value" => "FK", "name" => "Falkland Islands (Malvinas)" ), 		array( "value" => "FO", "name" => "Faroe Islands" ), 		array( "value" => "FJ", "name" => "Fiji" ), 		array( "value" => "FI", "name" => "Finland" ), 		array( "value" => "FR", "name" => "France" ), 		array( "value" => "GF", "name" => "French Guiana" ), 		array( "value" => "PF", "name" => "French Polynesia" ), 		array( "value" => "TF", "name" => "French Southern Territories" ), 		array( "value" => "GA", "name" => "Gabon" ), 		array( "value" => "GM", "name" => "Gambia" ), 		array( "value" => "GE", "name" => "Georgia" ), 		array( "value" => "DE", "name" => "Germany" ), 		array( "value" => "GH", "name" => "Ghana" ), 		array( "value" => "GI", "name" => "Gibraltar" ), 		array( "value" => "GR", "name" => "Greece" ), 		array( "value" => "GL", "name" => "Greenland" ), 		array( "value" => "GD", "name" => "Grenada" ), 		array( "value" => "GP", "name" => "Guadeloupe" ), 		array( "value" => "GU", "name" => "Guam" ), 		array( "value" => "GT", "name" => "Guatemala" ), 		array( "value" => "GG", "name" => "Guernsey" ), 		array( "value" => "GN", "name" => "Guinea" ), 		array( "value" => "GW", "name" => "Guinea-Bissau" ), 		array( "value" => "GY", "name" => "Guyana" ), 		array( "value" => "HT", "name" => "Haiti" ), 		array( "value" => "HM", "name" => "Heard Island and McDonald Islands" ), 		array( "value" => "VA", "name" => "Holy See (Vatican City State)" ), 		array( "value" => "HN", "name" => "Honduras" ), 		array( "value" => "HK", "name" => "Hong Kong" ), 		array( "value" => "HU", "name" => "Hungary" ), 		array( "value" => "IS", "name" => "Iceland" ), 		array( "value" => "IN", "name" => "India" ), 		array( "value" => "ID", "name" => "Indonesia" ), 		array( "value" => "IR", "name" => "Iran, Islamic Republic of" ), 		array( "value" => "IQ", "name" => "Iraq" ), 		array( "value" => "IE", "name" => "Ireland" ), 		array( "value" => "IM", "name" => "Isle of Man" ), 		array( "value" => "IL", "name" => "Israel" ), 		array( "value" => "IT", "name" => "Italy" ), 		array( "value" => "JM", "name" => "Jamaica" ), 		array( "value" => "JP", "name" => "Japan" ), 		array( "value" => "JE", "name" => "Jersey" ), 		array( "value" => "JO", "name" => "Jordan" ), 		array( "value" => "KZ", "name" => "Kazakhstan" ), 		array( "value" => "KE", "name" => "Kenya" ), 		array( "value" => "KI", "name" => "Kiribati" ), 		array( "value" => "KP", "name" => "Korea, Democratic People's Republic of" ), 		array( "value" => "KR", "name" => "Korea, Republic of" ), 		array( "value" => "KW", "name" => "Kuwait" ), 		array( "value" => "KG", "name" => "Kyrgyzstan" ), 		array( "value" => "LA", "name" => "Lao People's Democratic Republic" ), 		array( "value" => "LV", "name" => "Latvia" ), 		array( "value" => "LB", "name" => "Lebanon" ), 		array( "value" => "LS", "name" => "Lesotho" ), 		array( "value" => "LR", "name" => "Liberia" ), 		array( "value" => "LY", "name" => "Libya" ), 		array( "value" => "LI", "name" => "Liechtenstein" ), 		array( "value" => "LT", "name" => "Lithuania" ), 		array( "value" => "LU", "name" => "Luxembourg" ), 		array( "value" => "MO", "name" => "Macao" ), 		array( "value" => "MK", "name" => "Macedonia, the former Yugoslav Republic of" ), 		array( "value" => "MG", "name" => "Madagascar" ), 		array( "value" => "MW", "name" => "Malawi" ), 		array( "value" => "MY", "name" => "Malaysia" ), 		array( "value" => "MV", "name" => "Maldives" ), 		array( "value" => "ML", "name" => "Mali" ), 		array( "value" => "MT", "name" => "Malta" ), 		array( "value" => "MH", "name" => "Marshall Islands" ), 		array( "value" => "MQ", "name" => "Martinique" ), 		array( "value" => "MR", "name" => "Mauritania" ), 		array( "value" => "MU", "name" => "Mauritius" ), 		array( "value" => "YT", "name" => "Mayotte" ), 		array( "value" => "MX", "name" => "Mexico" ), 		array( "value" => "FM", "name" => "Micronesia, Federated States of" ), 		array( "value" => "MD", "name" => "Moldova, Republic of" ), 		array( "value" => "MC", "name" => "Monaco" ), 		array( "value" => "MN", "name" => "Mongolia" ), 		array( "value" => "ME", "name" => "Montenegro" ), 		array( "value" => "MS", "name" => "Montserrat" ), 		array( "value" => "MA", "name" => "Morocco" ), 		array( "value" => "MZ", "name" => "Mozambique" ), 		array( "value" => "MM", "name" => "Myanmar" ), 		array( "value" => "NA", "name" => "Namibia" ), 		array( "value" => "NR", "name" => "Nauru" ), 		array( "value" => "NP", "name" => "Nepal" ), 		array( "value" => "NL", "name" => "Netherlands" ), 		array( "value" => "NC", "name" => "New Caledonia" ), 		array( "value" => "NZ", "name" => "New Zealand" ), 		array( "value" => "NI", "name" => "Nicaragua" ), 		array( "value" => "NE", "name" => "Niger" ), 		array( "value" => "NG", "name" => "Nigeria" ), 		array( "value" => "NU", "name" => "Niue" ), 		array( "value" => "NF", "name" => "Norfolk Island" ), 		array( "value" => "MP", "name" => "Northern Mariana Islands" ), 		array( "value" => "NO", "name" => "Norway" ), 		array( "value" => "OM", "name" => "Oman" ), 		array( "value" => "PK", "name" => "Pakistan" ), 		array( "value" => "PW", "name" => "Palau" ), 		array( "value" => "PS", "name" => "Palestinian Territory, Occupied" ), 		array( "value" => "PA", "name" => "Panama" ), 		array( "value" => "PG", "name" => "Papua New Guinea" ), 		array( "value" => "PY", "name" => "Paraguay" ), 		array( "value" => "PE", "name" => "Peru" ), 		array( "value" => "PH", "name" => "Philippines" ), 		array( "value" => "PN", "name" => "Pitcairn" ), 		array( "value" => "PL", "name" => "Poland" ), 		array( "value" => "PT", "name" => "Portugal" ), 		array( "value" => "PR", "name" => "Puerto Rico" ), 		array( "value" => "QA", "name" => "Qatar" ), 		array( "value" => "RE", "name" => "Réunion" ), 		array( "value" => "RO", "name" => "Romania" ), 		array( "value" => "RU", "name" => "Russian Federation" ), 		array( "value" => "RW", "name" => "Rwanda" ), 		array( "value" => "BL", "name" => "Saint Barthélemy" ), 		array( "value" => "SH", "name" => "Saint Helena, Ascension and Tristan da Cunha" ), 		array( "value" => "KN", "name" => "Saint Kitts and Nevis" ), 		array( "value" => "LC", "name" => "Saint Lucia" ), 		array( "value" => "MF", "name" => "Saint Martin (French part)" ), 		array( "value" => "PM", "name" => "Saint Pierre and Miquelon" ), 		array( "value" => "VC", "name" => "Saint Vincent and the Grenadines" ), 		array( "value" => "WS", "name" => "Samoa" ), 		array( "value" => "SM", "name" => "San Marino" ), 		array( "value" => "ST", "name" => "Sao Tome and Principe" ), 		array( "value" => "SA", "name" => "Saudi Arabia" ), 		array( "value" => "SN", "name" => "Senegal" ), 		array( "value" => "RS", "name" => "Serbia" ), 		array( "value" => "SC", "name" => "Seychelles" ), 		array( "value" => "SL", "name" => "Sierra Leone" ), 		array( "value" => "SG", "name" => "Singapore" ), 		array( "value" => "SX", "name" => "Sint Maarten (Dutch part)" ), 		array( "value" => "SK", "name" => "Slovakia" ), 		array( "value" => "SI", "name" => "Slovenia" ), 		array( "value" => "SB", "name" => "Solomon Islands" ), 		array( "value" => "SO", "name" => "Somalia" ), 		array( "value" => "ZA", "name" => "South Africa" ), 		array( "value" => "GS", "name" => "South Georgia and the South Sandwich Islands" ), 		array( "value" => "SS", "name" => "South Sudan" ), 		array( "value" => "ES", "name" => "Spain" ), 		array( "value" => "LK", "name" => "Sri Lanka" ), 		array( "value" => "SD", "name" => "Sudan" ), 		array( "value" => "SR", "name" => "Suriname" ), 		array( "value" => "SJ", "name" => "Svalbard and Jan Mayen" ), 		array( "value" => "SZ", "name" => "Swaziland" ), 		array( "value" => "SE", "name" => "Sweden" ), 		array( "value" => "CH", "name" => "Switzerland" ), 		array( "value" => "SY", "name" => "Syrian Arab Republic" ), 		array( "value" => "TW", "name" => "Taiwan, Province of China" ), 		array( "value" => "TJ", "name" => "Tajikistan" ), 		array( "value" => "TZ", "name" => "Tanzania, United Republic of" ), 		array( "value" => "TH", "name" => "Thailand" ), 		array( "value" => "TL", "name" => "Timor-Leste" ), 		array( "value" => "TG", "name" => "Togo" ), 		array( "value" => "TK", "name" => "Tokelau" ), 		array( "value" => "TO", "name" => "Tonga" ), 		array( "value" => "TT", "name" => "Trinidad and Tobago" ), 		array( "value" => "TN", "name" => "Tunisia" ), 		array( "value" => "TR", "name" => "Turkey" ), 		array( "value" => "TM", "name" => "Turkmenistan" ), 		array( "value" => "TC", "name" => "Turks and Caicos Islands" ), 		array( "value" => "TV", "name" => "Tuvalu" ), 		array( "value" => "UG", "name" => "Uganda" ), 		array( "value" => "UA", "name" => "Ukraine" ), 		array( "value" => "AE", "name" => "United Arab Emirates" ), 		array( "value" => "GB", "name" => "United Kingdom" ), 		array( "value" => "US", "name" => "United States" ), 		array( "value" => "UM", "name" => "United States Minor Outlying Islands" ), 		array( "value" => "UY", "name" => "Uruguay" ), 		array( "value" => "UZ", "name" => "Uzbekistan" ), 		array( "value" => "VU", "name" => "Vanuatu" ), 		array( "value" => "VE", "name" => "Venezuela, Bolivarian Republic of" ), 		array( "value" => "VN", "name" => "Viet Nam" ), 		array( "value" => "VG", "name" => "Virgin Islands, British" ), 		array( "value" => "VI", "name" => "Virgin Islands, U.S." ), 		array( "value" => "WF", "name" => "Wallis and Futuna" ), 		array( "value" => "EH", "name" => "Western Sahara" ), 		array( "value" => "YE", "name" => "Yemen" ), 		array( "value" => "ZM", "name" => "Zambia" ), 		array( "value" => "ZW", "name" => "Zimbabwe" ) 	];
}

if(isset($_POST["name"])){
	$name = $_POST["name"];
	$email = $_POST["email"];
	$phone = $_POST["phone"];
	$country = $_POST["country"];
	$reason = $_POST["reason"];
	$description = $_POST["description"];

	$data = [
		'name' => $name,
		'email' => $email,
		'phone' => $phone,
		'country' => $country,
		'reason' => $reason,
		'description' => $description,
	];
$query = $db->prepare("INSERT INTO contact_form SET
	name = :name,
	email = :email,
	phone = :phone,
	country = :country,
	reason = :reason,
	description = :description
")->execute($data);

	if($query){
		$type = "0";
		$t = "Message sent succefully!, We will get back to you in 48hours. Thanks.";
	} else{
		$type = "1";
		$t = "Message not sent, please try again!";
	}
	print json_encode( array(
		"type" => $type,
		"t" => $t
		)
	);
exit();
}

if(isset($_GET["kl"]) && $_GET["kl"]=="r00"){
	$t = @file_get_contents("client_contact_form0.php");
	print json_encode( array(
		"type" => 0,
		"count" => 1,
		"t" => str_replace("#countries",optionsbvac(),$t),
		)
	);
exit();
}

if(isset($_POST["y001"])){
	$fname = $_POST["y001"];
	$lname = $_POST["y002"];
	$email = $_POST["y003"];
	$phone = $_POST["y004"];
	$location1 = $_POST["y005"];
	$location2 = $_POST["y006"];
	$type_of_service = $_POST["y007"];
	$code = uniqid();
	$data = [
		'code' => $code,
		'fname' => $fname,
		'lname' => $lname,
		'email' => $email,
		'phone' => $phone,
		'country' => $location1 ."|". $location2,
		'type_of_service' => $type_of_service,
	];
	/*$query = $db->prepare("INSERT INTO project_request_form_for_webdev SET
		code = :code,
		f_name = :fname,
		l_name = :lname,
		email = :email,
		phone = :phone,
		country = :country,
		type_of_service = :type_of_service
	")->execute($data);*/
	//if($query){
		//$_SESSION["cddddi"]=$code;
	print json_encode([
		"type" => 0,
		"count" => 2,
		"t" => str_replace("#coded",$code, @file_get_contents("client_contact_form_2.php"))
	]);
	//}
exit();
}

if(isset($_POST["j001"])){
	$interest = $_POST["j001"];
	$type_of_biz = $_POST["j002"];
	$budget = $_POST["j003"];
	$project_start_date = $_POST["j004"];
	$project_description = $_POST["j005"];
	$phone = $_POST["j006"];
	$email = $_POST["j007"];
	$form_submit_date = date("M/D Y-m-d :-: H:i:s a");
	$code = $_POST["j008"];
	$data = [
		'interest' => $interest,
		'type_of_biz' => $type_of_biz,
		'budget' => $budget,
		'project_start_date' => $project_start_date,
		'project_description' => $project_description,
		'how_to_contact' => $phone ."|". $email,
		'form_submit_date' => $form_submit_date,
		'code' => $code,
	];
	/*$query = $db->prepare("UPDATE project_request_form_for_webdev SET
		interest = :interest,
		type_of_biz = :type_of_biz,
		budget = :budget,
		project_start_date = :project_start_date,
		project_description = :project_description,
		how_to_contact = :how_to_contact,
		form_submit_date = :form_submit_date
		WHERE
		code = :code
	")->execute($data);*/
	//if($query){
	print json_encode([
		"type" => 0,
		"count" => 3,
		"t" => "<div class=\"alert alert-success\" style=\"margin-top:50px;\">Your request has been submitted and is currently reviewed. You will be contacted once concluded.</div>",
	]);
	//}
	session_destroy($_SESSION["cddddi"]);
		$_SESSION["cddddi"]="";
exit();
}


