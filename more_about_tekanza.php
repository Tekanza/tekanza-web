<head>
<style>
.more_about{
	background-color:lightblue;
	width:85%;
	text-align:justify;
	margin-top:40px;
	margin-left:60px;
	border-radius:40px;
	clear:both;
	float:center;
	padding-left:40px;
	padding-right:40px;
	padding-top:15px;
	padding-bottom:40px;
	overflow:hidden;
	line-height:1.7em;		
}
.more_about a{
	color:#0000ff;
}
 .more_about,:link{
	text-decoration:none;
}
.more_about ol > li a:hover{
	color:#900;
}
</style>
</head>
<div class="more_about">
<p>Tekanza ICT Hub is a sophisticated network infrastructure company designed to promote and increase the productivity of any enterprise through quality network services delivery.</p>
<p>As a business enterprise, government, and any institutions, understanding the importance of network infrastructure design and developing an implementation plan based on the needs and priorities of your firm is very crucial to long-term success.
Therefore, hiring and partnering with Tekanza ICT Hub will provides a reliable network infrastructure plan to you while giving the flexibility to outsource and augment your company services optimally. 
Our team is well equipped with the infrastructural design and development and they can manage your network infrastructure design and implementation from the ground up, linking and connecting multiples network infrastructural devices, bringing locations and powering the business systems your company needs to build.</p>
<p>Our network infrastructure consultants and engineers have years of experience in architecting, managing network cloud system, servers  and client infrastructure, on-premises, etc that are designed to the specific business goals and models. Every infrastructure designed is uniquely given to each client aims at individual goals and objectives for proper optimized operations.
Tekanza ICT Hub has an expertise in development and implementing solutions.</p>
<p>The area of specialties includes;</p>
<p>
<ol>
<li><a href="#">Implementing and managing of cloud computing and storage.</a></li>
<li><a href="#">Server and Client installation</a></li>
<li><a href="#">On-premise server computing and storage</a></li>
<li><a href="#">Cabling infrastructure</a></li>
<li><a href="#">Physical and logical connectivity</a></li>
<li><a href="#">Wireless installation and management</a></li>
<li><a href="#">Business continuity, disaster recovery and data back-up</a></li>
<li><a href="#">Office relocation or new location built-out</a></li>
<li><a href="#">Geo location</a></li>
<li><a href="#">VOIP and on-premises telephone system</a></li>
<li><a href="#">Centralized prints services</a></li>
<li><a href="#">Application integration</a></li>
<li><a href="#">Acquisition integration</a></li>
<li><a href="#">Email services</a></li>
<li><a href="#">Firewall, antivirus, antispam and other data security and protection hardware and software.</a></li>
</ol>
</p>
 </div>