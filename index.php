<?php include"query.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Home</title>
<meta charset="utf-8">
<meta name="description" content="Tekanza ICT Hub">
<meta name="keywords" content="Web Development, App Development, Network Infrastructure, Branding & Identity">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="css/about.css">
<link rel="stylesheet" href="css/about_responsive.css">
<link type="text/css" rel="stylesheet" href="css/lightslider.css">

<style>
    img.blended-feed{ box-shadow:0 0 55px #000 inset !important;}ul li{list-style:none !important;}
</style>

<?php head();?>
<script src="preloader.js"></script>
    <div class="site-blocks-cover overlay" style="background-image: url(images/tek-bg-img_1.png); background-position:center; background-size: auto; height: 525px; background-repeat:no-repeat;" data-aos="fade">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">
          <div class="col-md-12" data-aos="fade-up" data-aos-delay="400">                
            <div class="row justify-content-center mb-4">
              <div class="col-md-8 text-center">
                <h1><span class="typed-words"></span></h1>
                <p class="lead mb-2"></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
   <section class="section ft-feature-1">
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-12 bg-black w-100 ft-feature-1-content">
            <div class="row algn-items-center">
              <div class="col-lg-5">
                <div class="h-100">
                  <div class="mb-5 d-flex align-items-center">
                    <div>
                      <a href="#/-https://vimeo.com/317571768" class="popup-vimeo d-block play"><span class="icon-play"></span></a>
                    </div>
                    <h2>Welcome To Tekanza ICT Hub</h2>
                  </div>
                  <img src="images/vim_img.jpg" alt="Image" class="img-feature img-fluid blended-feed">
                </div>
              </div>
              <div class="col-lg-3 ml-auto">
                <div class="mb-4">
                  <h3 class="d-flex align-items-center"><i class="fa fa-globe"><strong> We Create</strong> </i></h3>
                  <p>We bring into existence - the reality of conceived new ideas in order to make them useful. How do we achieve these? We observe, inquire, mirror, name, question, challenge, and reframe information and experience. </p>
                </div>
                <div class="mb-4">
                  <h3 class="d-flex align-items-center"><i class="fa fa-globe"><strong> We Innovate</strong> </i></h3>
                  <p>We ensure value is created and added to the community and the uniqueness of our products by deploying the best models and techniques in profering solutions.  </p>
                </div>
                <div class="mb-4">
                  <h3 class="d-flex align-items-center"><i class="fa fa-connectdevelop"><strong> We Develop</strong> </i></h3>
                  <p>We grow Ideas, Talents and Resources that are carefully and systematically geared towards their absorption and utilization in the growth of our society and the community.</p>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="mb-4">
                  <h3 class="d-flex align-items-center"><span class="icon icon-phonelink mr-2"></span><span><stong></stong> We Integrate</span></h3>
                  <p>We employ a holistic approach to process design, collaboration and communication, using the best tools available, hence ensuring a more candid approach to blend as functioning units. </p>
                </div>
                <div>
                  <h3 class="d-flex align-items-center"><span class="icon icon-question_answer mr-2"></span><span><strong> We Explore</strong></span></h3>
                  <p>We explore new ideas, innovations, resources, developments and areas of needs in our environment, to properly integrate into our system of operations.  </p>
                </div>
               </div>
            <div class="col-12 text-center mt-5">
            <a href="about" class="btn btn-primary btn-md">Read More</a>
          </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4">
            <div class="p-3 box-with-humber">
              <h2 style="color:#000; text-align:center;">Vision</h2>
              <p><center>To become Nigerian and African most priced technological development hub.</center></p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4">
            <div class="p-3 box-with-humber">
              <h2 style="color:#000; text-align:center;">Our Mission</h2>
              <p><center>To build, solve and find solutions to problems in our environment through technological enterprise, development and initiatives.</center></p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4">
            <div class="p-3 box-with-humber">
              <h2 style="color:#000; text-align:center;">Our Values</h2>
              <p><center>Innovation, Diversity, Teamwork, Accountability, Integrity, Impact and Quality.</center></p>
            </div>
          </div>
        </div>
      </div>
    </section>
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-8 text-center">
            <h2 class="text-black h1 mt-3 py-3 site-section-heading text-center">Featured Projects</h2>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 col-lg-4">
            <a href="#" class="media-1">
              <img src="images/img_h_1.png" class="img-fluid">
              <div class="media-1-content">
                <h2><i class="fa fa-hourglass-half" style="font-size:55px"></i></h2>
                <span class="category"><i class="fa fa-gears" style="font-size:80px"></i></span>
              </div>
            </a>
          </div>
            <div class="col-md-6 col-lg-4">
            <a href="#" class="media-1">
              <img src="images/img_h_1.png" alt="" class="img-fluid">
              <div class="media-1-content">
                <h2><i class="fa fa-hourglass-half" style="font-size:55px"></i></h2>
                <span class="category"><i class="fa fa-gears" style="font-size:80px"></i></span>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-4">
            <a href="#" class="media-1">
              <img src="images/img_h_1.png" alt="" class="img-fluid">
              <div class="media-1-content">
                <h2><i class="fa fa-hourglass-half" style="font-size:55px"></i></h2>
                <span class="category"><i class="fa fa-gears" style="font-size:80px"></i></span>
              </div>
            </a>
          </div>
          <div class="col-12 text-center mt-5">
            <a href="#Show_All_Works/" class="btn btn-primary btn-md">Show All Works</a>
          </div>
        </div>
      </div>
    <section class="site-section testimonial-wrap">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 text-center">
            <h2 class="text-black h1 my-3 py-3 site-section-heading text-center">Our Developmental Trajectories and Interests</h2>
        <div class="container-fluid">
        <div class="row">
		 <div class="col-md-6 col-lg-4">
		  <a class="media-1">
              <img src="images/tek_img_images/iot_5.png" alt="Internet of Things (IoT)" class="img-fluid" title="Internet of Things (IoT)" >
              <div class="media-1-content">
                <small><strong>Internet of Things (IoT)</strong></small>
              </div>
			</a>
          </div>
          <div class="col-md-6 col-lg-4">
            <a class="media-1">
              <img src="images/tek_img_images/ai_3.png" alt="Artificial Intelligence (AI)" class="img-fluid" title="Artificial Intelligence (AI)">
              <div class="media-1-content">
                <small><strong>Artificial Intelligence (AI)</strong></small>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-4">
            <a class="media-1">
              <img src="images/tek_img_images/bio_2.png" alt="Biotechnology" class="img-fluid" title="Biotechnology">
              <div class="media-1-content">
                <small><strong>Biotechnology</strong></small>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-4">
            <a class="media-1">
              <img src="images/tek_img_images/data_sc2.png" alt="Data Science</" class="img-fluid" title="Data Science">
              <div class="media-1-content">
                <small><strong>Data Science</strong></small>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-4">
            <a class="media-1">
              <img src="images/tek_img_images/cld_com_2.png" alt="Cloud Computing" class="img-fluid" title="Cloud Computing">
              <div class="media-1-content">
                <small><strong>Cloud Computing</strong></small>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-4">
            <a class="media-1">
              <img src="images/tek_img_images/blk_chain_2.png" alt="Blockchain" class="img-fluid" title="Blockchain">
              <div class="media-1-content">
                <small><strong>Blockchain</strong></small>
              </div>
            </a>
          </div>
		 </div>
		</div>
		</div>
        </div>
      </div>
</section>
<div class="container d-none d-md-block d-lg-block my-5">
    <h2 class="text-black h1 site-section-heading pb-3 text-center">Partners</h2>
    <div class="d-flex flex-row justify-content-center">
          <div class="p-2 bd-highlight">
            <img src="images/tek_img_images/prt_img_logo/prt-cis (2).png" alt="Partner_logo_1" title="Partners" style="width: 100%">
          </div>
          <div class="p-2 bd-highlight">
              <img src="images/tek_img_images/prt_img_logo/prt_gd.png" alt="Partner_logo_1" title="Partners" style="width: 100%">
          </div>
         <div class="p-2 bd-highlight">
            <img src="images/tek_img_images/prt_img_logo/prt-cis (2).png" alt="Partner_logo_1" title="Partners" style="width: 100%">
          </div>
          <div class="p-2 bd-highlight">
              <img src="images/tek_img_images/prt_img_logo/prt_gd.png" alt="Partner_logo_1" title="Partners" style="width: 100%">
          </div>
          <div class="p-2 bd-highlight">
            <img src="images/tek_img_images/prt_img_logo/prt-cis (2).png" alt="Partner_logo_1" title="Partners" style="width: 100%">
          </div>
          <div class="p-2 bd-highlight">
              <img src="images/tek_img_images/prt_img_logo/prt_gd.png" alt="Partner_logo_1" title="Partners" style="width: 100%">
          </div>
         <div class="p-2 bd-highlight">
            <img src="images/tek_img_images/prt_img_logo/prt-cis (2).png" alt="Partner_logo_1" title="Partners" style="width: 100%">
          </div>
          <div class="p-2 bd-highlight">
              <img src="images/tek_img_images/prt_img_logo/prt_gd.png" alt="Partner_logo_1" title="Partners" style="width: 100%">
          </div>
    </div>
</div>
<div class="container d-block d-sm-block d-md-none my-3">
    <div class="row">
        <div class="col-12">
            <h2 class="text-black h1 site-section-heading text-center">Partners</h2>
            <ul id="lightSlider">
                <li class="item-a">
                    <img src="images/tek_img_images/prt_img_logo/prt_gd.png" alt="Partner_logo_1" title="Partners" style="width: 100%">
                </li>
                  <li class="item-b">
                      <img src="images/tek_img_images/prt_img_logo/prt-cis (2).png" alt="Partner_logo_1" title="Partners" style="width: 100%">
                  </li>
                  <li class="item-c">
                      <img src="images/tek_img_images/prt_img_logo/prt_gd.png" alt="Partner_logo_1" title="Partners" style="width: 100%">
                  </li>
            </ul>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/lightslider.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#lightSlider").lightSlider({
        item: 3,
        autoWidth: false,
        slideMove: 1, // slidemove will be 1 if loop is true
        slideMargin: 10,
 
        addClass: '',
        mode: "slide",
        useCSS: true,
        cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
        easing: 'linear', //'for jquery animation',////
 
        speed: 400, //ms'
        auto: false,
        loop: false,
        slideEndAnimation: true,
        pause: 2000
    });
});
</script>


<script src="js/typed.js"></script>
<script>
var typed = new Typed('.typed-words', {
strings: ["We are Experts in Building Network Infrastructures", "We are Experts in Web and Software Developments", "We build and Manage Database Systems", "We are Experts in Digital Marketing, Branding &amp; Identity", "We are Expert in ICT Research Development", "&amp; Lots more..."],
typeSpeed: 80,
backSpeed: 80,
backDelay: 4000,
startDelay: 1000,
loop: true,
showCursor: true
});
</script>
<?php footer();?>