
  <div class="site-wrap">
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
    <div class="border-bottom top-bar py-2">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <p class="mb-00">
             <span><a href="tel:09060976211" class="tek-blue p-1"><i class="fa fa-phone-square" aria-hidden="true"></i> +2349060976211</a></span>
              <span><a href="mailto:info@tekanza.com" class="tek-blue p-1"><i class="fa fa-envelope-square" aria-hidden="true"></i> info@tekanza.com</a></span>
            </p>
          </div>
          <div class="col-md-6">
            <ul class="social-media py-2">
              <li><a href="https://fb.com/tekanz" class="tek-blue p-2"><span class="icon-facebook" ></span></a></li>
              <li><a href="https://twitter.com/TekanzaLLC" class="tek-blue p-2"><span class="icon-twitter" ></span></a></li>
              <li><a href="https://instagram.com/tekanzaofficial" class="tek-blue p-2"><span class="icon-instagram" ></span></a></li>
              <li><a href="https://linkedin.com/tekanzaLLC" class="tek-blue p-2"><span class="icon-linkedin" ></span></a></li>
            </ul>
          </div>
        </div>
      </div> 
    </div>
    <header class="site-navbar py-1 text-white" role="banner">
      <div class="container p-0">
        <div class="row mx-0 my-0">
          <div class="col-2 d-inline-block d-lg-none ml-md-0 py-3" style="position: relative; top: 10px;"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu h3"></span></a></div>
          <div class="col-10 d-lg-none"><a href="index" class="text-black h2 mb-0"><img class="mb-0 site-logo" src="images/tek_3_4.png" title="Tekanza" alt="Tekanza" style="width: 100%"></a></div>
        </div>
        <div class="row text-white">
          <div class="col-12 col-md-12 d-none d-lg-block">
            <nav class="site-navigation clearfix navbar navbar-expand-lg position-relative" role="navigation">
                <a class="navbar-brand" href="index">
                    <img src="images/tek_3_4.png" width="150" alt="" loading="lazy">
                </a>
              <ul class="site-menu d-flex justify-content-end js-clone-nav d-none d-lg-block">
                <li class="act"><a href="index">Home</a></li>
				<li><a href="about">About Us</a></li>
                <li><a href="portfolio">Portfolio</a></li>
                <li class="has-children">
                  <a href="#">Our Services</a>
                  <ul class="dropdown">
				   <li><a href="more_about_tekanza_web" target="_blank">Web Development</a></li>  
                    <li><a href="#our-bonuses">Bonus Services Program</a></li>
                    <li><a href="#More_services/">......more Services</a></li>
                  </ul>
                </li>
                <li><a href="#Partners">Partners</a></li>
                <li class="has-children">
                  <a href="#">Explore</a>
                  <ul class="dropdown">
				   <li><a href="#blog" target="_blank">Blog</a></li>  
                    <li><a href="#careers/">Careers</a></li>
                    <li><a href="#foundation/">Our Foundation</a></li>
                  </ul>
                </li>
			 <li><a href="contact">Contact</a></li>
              </ul>
            </nav>
          </div>
      </div>
    </div>
</header>